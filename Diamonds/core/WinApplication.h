#pragma once
#include <windows.h>

#define WIN32_LEAN_AND_MEAN

class WinApplication
{
public:
	WinApplication(void);
	virtual ~WinApplication(void);
	
	HINSTANCE &GetAppInstance()		{ return appInstance;	}
	HWND &GetAppHwnd()						{ return appWindow;		}

protected:
	virtual void GetWindowDims( unsigned int &width, unsigned int &height )		{ width = 680; height = 680; }
	virtual LPCSTR GetWinTitle()												{ return "Diamond App"; }

	virtual bool AppPreBegin()		{ return true; }
	virtual bool AppBegin()			{ return true; }
	virtual bool AppUpdate()		{ return true; }
	virtual bool AppEnd()			{ return true; }

	virtual void Paint( HWND AppHwnd, WPARAM Wparam, LPARAM Lparam ) { }
	virtual void SysCommand( HWND AppHwnd, WPARAM Wparam, LPARAM Lparam );
	virtual void ProcessMouseInput(HWND AppHwnd, UINT msg, WPARAM Wparam, LPARAM Lparam) { }
	virtual void ProcessKeyboardInput(HWND AppHwnd, UINT msg, WPARAM Wparam, LPARAM Lparam) { }
	
	virtual void ProcessMessage( UINT msg, WPARAM Wparam, LPARAM Lparam ) { }

	void CloseWindow();

private:
	HRESULT InitWindow();
	int InternalWinMain(HINSTANCE hInstance);

	friend int WINAPI WinMain( HINSTANCE, HINSTANCE, TCHAR *, int );
	static LRESULT CALLBACK WndProc( HWND Hwnd, UINT msg, WPARAM wParam, LPARAM lParam );

private:
	static WinApplication *pApp;
	HINSTANCE		appInstance;
	HWND			appWindow;
	const TCHAR		*CLASSNAME;

};
