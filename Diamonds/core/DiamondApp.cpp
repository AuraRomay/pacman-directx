#include "DiamondApp.h"
#include <string>
#include "..\render\DXCore.h"

#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)
const float MOUSE_ROTATE_SPEED = 0.30f;
int x = 0;

DiamondApp pApp;

DiamondApp::DiamondApp(void)
	: WinApplication(), mpDXCore(0)
	//, mHead(0.f), mPitch(0.f)
{

}

DiamondApp::~DiamondApp(void)
{
	if(mpDXCore) { delete mpDXCore; mpDXCore = 0; }
}

void DiamondApp::ReleaseObjects()
{
	
}

void DiamondApp::ProcessInput(float elapsedTime)
{
	static UCHAR pKeyBuffer[ 256 ];
	if ( !GetKeyboardState( pKeyBuffer ) ) return;

	const float sleept = 0.1f;
	static float ontime = 0.f;
	static bool iswait = false;

	if(iswait) {
		ontime += elapsedTime;
		if(ontime > sleept) {
			iswait = false;
			ontime = 0.f;
		} else { return; }
	}

	if(KEY_DOWN(VK_RIGHT)) {
		//mSceneMgr.rotateZTop();
		iswait = true;
	}
	if(KEY_DOWN(VK_LEFT)) {
		iswait = true;
	}
	if(KEY_DOWN(VK_UP)) {
		//mSceneMgr.rotateXRight();
		iswait = true;
	}
	if(KEY_DOWN(VK_DOWN)) {
		iswait = true;
	}
	if (KEY_DOWN(VK_SPACE)) {
		iswait = true;
	}

	if (KEY_DOWN(0x57)) {
		iswait = true;
	}

	if (KEY_DOWN(0x41)) {
		iswait = true;
	}

	if (KEY_DOWN(0x53)) {
		iswait = true;
	}

	if (KEY_DOWN(0x44)) {
		iswait = true;
	}
}

void DiamondApp::ProcessMouseInput(HWND AppHwnd, UINT msg, WPARAM Wparam, LPARAM Lparam)
{
	enum MouseMode { MOUSE_NONE, MOUSE_TRACK, MOUSE_DOLLY, MOUSE_ROTATE };

	static MouseMode mode = MOUSE_NONE;
	static POINT ptMousePrev = { 0 };
	static POINT ptMouseCurrent = { 0 };
	static int mouseButtonsDown = 0;
	static float dx = 0.0f;
	static float dy = 0.0f;
	static float wheelDelta = 0.0f;
	static float sHead = 0.f, sPitch = 0.f;

	switch (msg)
	{
	case WM_LBUTTONDOWN:
		mode = MOUSE_ROTATE;
		++mouseButtonsDown;
		SetCapture(AppHwnd);
		ptMousePrev.x = static_cast<int>(static_cast<short>(LOWORD(Lparam)));
		ptMousePrev.y = static_cast<int>(static_cast<short>(HIWORD(Lparam)));
		ClientToScreen(AppHwnd, &ptMousePrev);
		break;
	case WM_MOUSEMOVE:
		ptMouseCurrent.x = static_cast<int>(static_cast<short>(LOWORD(Lparam)));
		ptMouseCurrent.y = static_cast<int>(static_cast<short>(HIWORD(Lparam)));
		ClientToScreen(AppHwnd, &ptMouseCurrent);

		switch (mode)
		{
		case MOUSE_ROTATE:
			dx = static_cast<float>(ptMousePrev.x - ptMouseCurrent.x);
			dx *= MOUSE_ROTATE_SPEED;

			dy = static_cast<float>(ptMousePrev.y - ptMouseCurrent.y);
			dy *= MOUSE_ROTATE_SPEED;

			sHead -= dx;
			sPitch += dy;

			if (sPitch > 90.0f)
				sPitch = 90.0f;

			if (sPitch < -90.0f)
				sPitch = -90.0f;

			mSceneMgr.ChangeModelRotation(sHead, sPitch);
			break;
		}
		ptMousePrev.x = ptMouseCurrent.x;
		ptMousePrev.y = ptMouseCurrent.y;
		break;
	case WM_LBUTTONUP:
		if (--mouseButtonsDown <= 0)
		{
			mouseButtonsDown = 0;
			mode = MOUSE_NONE;
			ReleaseCapture();
		}
		else
		{
			if (Wparam & MK_LBUTTON)
				mode = MOUSE_TRACK;
			else if (Wparam & MK_RBUTTON)
				mode = MOUSE_ROTATE;
			else if (Wparam & MK_MBUTTON)
				mode = MOUSE_DOLLY;
		}
		break;
	default:
		break;
	}
}

void DiamondApp::ProcessKeyboardInput(HWND AppHwnd, UINT msg, WPARAM Wparam, LPARAM Lparam)
{
	switch (static_cast<int>(Wparam))
	{
	case VK_ESCAPE:
		PostMessage(AppHwnd, WM_CLOSE, 0, 0);
		break;

	case VK_SPACE:
		break;

	case VK_BACK:
		break;

	case 'H':
	case 'h':
			mSceneMgr.ChangeFirstModelPos(1.f, 0.f,0);
	
		break;

	case 'F':
	case 'f':
		mSceneMgr.ChangeFirstModelPos(-1.f, 0.f,0);
		break;

	case 'T':
	case 't':
		mSceneMgr.ChangeFirstModelPos(0.f, 1.f,0);
		break;

	case 'G':
	case 'g':
		mSceneMgr.ChangeFirstModelPos(0.f, -1.f,0);
		break;

	case 'W':
	case 'w':
		if (ver1 == false) {
			
			mSceneMgr.cam = false;
			ver1 = true;
		}
		else if (ver1 == true) {
			
			mSceneMgr.cam = true;
			ver1 = false;
		}
		break;
	}
}



bool DiamondApp::AppBegin()
{	
	unsigned int w = 0, h = 0;
	GetWindowDims(w, h);

	mpDXCore = new DXCore();
	mpDXCore->Initialize(GetAppHwnd(), w, h);
	
	InitializeScene();
	BuildObjects();

	return true;
}

void DiamondApp::InitializeScene()
{
	mSceneMgr.Initialize(mpDXCore->Get3DDevice(), mpDXCore->GetDeviceContext());
}

void DiamondApp::BuildObjects()
{
}

void DiamondApp::GhostMovement()
{
	//if (x < 10) {

	mSceneMgr.ChangePos(0.f, -0.002f, 2);
		//x++;
	//}
	//else if (x > 10) {
	mSceneMgr.ChangePos(-0.005f, 0.f, 1);
	//	x--;
	//}
	//mSceneMgr.ChangePos(-0.005f, 0.f, 2);
}

bool DiamondApp::AppUpdate()
{
	mTimer.Tick();
	float elapsedTime = mTimer.GetTimeElapsed();

	ProcessInput(elapsedTime);

	const float colorClear[4] = { 0.f, 0.f, 0.f, 1.f };
	mpDXCore->ClearColor(colorClear);
	GhostMovement();

	mSceneMgr.update(elapsedTime);
	mSceneMgr.draw();

	mpDXCore->Present();	
	return true;
}

bool DiamondApp::AppEnd()
{
	mpDXCore->Close();
	return true;
}


