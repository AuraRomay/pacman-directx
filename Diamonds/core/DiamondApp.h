#pragma once
#include "winapplication.h"
#include "../scene/SceneMgr.h"
#include "time/Timer.h"

class DXCore;


class DiamondApp : public WinApplication
{
public:
	DiamondApp(void);
	virtual ~DiamondApp(void);

private:
	virtual bool AppBegin();
	virtual bool AppUpdate();
	virtual bool AppEnd();

	void InitializeScene();

	void BuildObjects();
	void ReleaseObjects();
	void ProcessInput(float elapsedTime);
	
	void ProcessMouseInput(HWND AppHwnd, UINT msg, WPARAM Wparam, LPARAM Lparam) override;
	void ProcessKeyboardInput(HWND AppHwnd, UINT msg, WPARAM Wparam, LPARAM Lparam) override;

	void GhostMovement();

private:
	CTimer mTimer;
	DXCore *mpDXCore;

	CSceneMgr mSceneMgr;

public:
	bool ver1 = false;

	//float mHead, mPitch;
};
