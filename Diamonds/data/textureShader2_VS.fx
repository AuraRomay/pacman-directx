cbuffer simpleConstantBuffer : register(b0)
{
	matrix model;
	matrix view;
	matrix projection;
};

struct VertexInputType
{
    float4 position : POSITION;
		float3 normal : NORMAL;
    float2 tex : TEXCOORD0;
	//float3 tangent : TANGENT;
	//float3 binormal : BINORMAL;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
 //  	float3 normal : NORMAL;
	//float3 tangent : TANGENT;
	//float3 binormal : BINORMAL;
};


////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelInputType main(VertexInputType input)
{
    PixelInputType output = (PixelInputType)0;

	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, model);
    output.position = mul(output.position, view);
    output.position = mul(output.position, projection);
    
	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;
    
    // Calculate the normal vector against the world matrix only and then normalize the final value.
 //   output.normal = mul(input.normal, (float3x3)worldMatrix);
 //   output.normal = normalize(output.normal);

	//// Calculate the tangent vector against the world matrix only and then normalize the final value.
 //   output.tangent = mul(input.tangent, (float3x3)worldMatrix);
 //   output.tangent = normalize(output.tangent);

 //   // Calculate the binormal vector against the world matrix only and then normalize the final value.
 //   output.binormal = mul(input.binormal, (float3x3)worldMatrix);
 //   output.binormal = normalize(output.binormal);

    return output;
}