cbuffer simpleConstantBuffer : register(b0)
{
		matrix model;
		matrix view;
		matrix projection;
};

struct VS_OUTPUT
{
		float4 Pos : SV_POSITION;
};

VS_OUTPUT main( float4 Pos : POSITION )
{
		VS_OUTPUT output = (VS_OUTPUT)0;
		output.Pos = mul( Pos, model );
		output.Pos = mul( output.Pos, view );
		output.Pos = mul( output.Pos, projection );
		return output;
}


