#pragma once
#include <vector>
#include "../model/BaseFlake.h"

enum class FaceType {
	ZFRONT = 0,
	XRIGHT,
	CENTER,
};

class CRotationBase
{
public:
	~CRotationBase(void)		{ }
	CRotationBase() { }
	virtual void setFlakesVector(FaceType type, std::vector<std::vector<CBaseFlake*>> vFlakes) = 0;
};

class CRotationFace : public CRotationBase
{
public:
	~CRotationFace(void)	{ }
	CRotationFace() { }

	void setFlakesVector(FaceType type, std::vector<std::vector<CBaseFlake*>> vFlakes) override;
	void rotateRight();

private:
	CBaseFlake *mUsedFlakes[21];
	phyVector3D mSavedColors[21];
};

