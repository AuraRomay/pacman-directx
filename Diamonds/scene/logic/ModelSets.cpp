#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "ModelSets.h"

std::mutex CModelSets::_mutex;

std::shared_ptr<CModelSets>& CModelSets::getInstance()
{
	static std::shared_ptr<CModelSets> instance = nullptr;
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)
			instance.reset(new CModelSets());
	}
	return instance;
}

CModelSets::CModelSets()
{
	stackPos = { -40.f, 0.f, 2.f };
	for (int i = 0; i < 7; ++i) {
		deskPos[i] = { -30.f +(6.f *i), 0.f, 2.f };
		deskBusy[i] = false;
	}
	gravePos = { 17.f, 0.f, 2.f };
}


void CModelSets::insertBall(float S0x, float S0y, float V0x, float V0y)
{
	int index = mvBallPositions.size() + 1;
	sModelState model = { { S0x, S0y }, { V0x, V0y }, { S0x, S0y }, { RADIOBALL }, { 0.f } };
	mvBallPositions.insert(std::make_pair(index, model));
	//mvBallPositions.push_back({ { S0x, S0y }, { V0x, V0y }, { S0x, S0y }, { RADIOBALL }, { 0.f } });
}

void takeCardFromStack()
{

}

void CModelSets::setToBeDeleted(int index)
{
	mvBallKeysToDelete.push_back(index);
}

void CModelSets::insertPin(float S0x, float S0y)
{
	mvPinPositions.push_back({ { S0x, S0y }, { RADIOPIN } });
}

void CModelSets::insertSegment(float SAx, float SAy, float SBx, float SBy)
{
	mvSegmentPositions.push_back({ { SAx, SAy }, { SBx, SBy } });
}

void CModelSets::insertDrain(float SAx, float SAy, float size)
{
	mvDrainPositions.push_back({ { SAx, SAy }, { SAx + size, SAy } });
}

void CModelSets::insertTexture(int id, int startIndex)
{
	sTextureState state = { startIndex, true };
	mvTextureStates.insert(std::make_pair(id, state));
}

void CModelSets::setModelGravity(float g)
{
	mPhysics.setGravity(g);
}

void CModelSets::setTextureActive(int id, bool isActive)
{
	if (mvTextureStates.count(id) != 0)
	{
		sTextureState &state =	mvTextureStates.at(id);
		state.isActive = isActive;
	}
}

void CModelSets::insertScoreNum(int number)
{
	mvScorePositions.clear();
	if (number <= 0)
	{
		mvScorePositions.push_back(0);
		return;
	}
	unsigned int base = 10;
	const unsigned int length = static_cast<int>(floor(log10(number)) + 1);
	for (unsigned int i = 0; i < length; i++)
	{
		mvScorePositions.push_back(number % base);
		number /= base;
	}
}

void CModelSets::insertCard(int id)
{
	sCardState cardState = { id, 0.f, 0.f, 0, 0, true };
	mvCardStates.push_back(cardState);
	float Sx = stackPos.x;
	float Sy = stackPos.y;
	float Sz = stackPos.z;
	sCardMoveState cardMoveState = { id, { Sx, Sy, Sz }, 
		0.f, 	0.f, 	1.f, 	1.f, 	{ Sx, Sy, Sz },	0.f,
		true, CS_IDLE };

	mvCardMoveStates.push_back(cardMoveState);

}

void CModelSets::setCardActive(int id, bool isActive)
{
	for (auto &card : mvCardStates) {
		if (card.id == id) {			
			card.isActive = isActive;
		}
	}
}

void CModelSets::setCardAnimation(int id, int frameSize, float time)
{
	for (auto &card : mvCardStates) {
		if (card.id == id) {
			card.frameSize = frameSize;
			card.deltaTime = time;
			card.actualTime = 0.f;
			card.actualFrame = 0;
		}
	}
}

void CModelSets::update(float deltaTime)
{
	for (auto &ballState : mvBallPositions)
		mPhysics.CalculateStep(deltaTime, ballState.second);

	for (auto &cardState : mvCardStates)
		mPhysics.CalculateStep(deltaTime, cardState);

	calculateCollisions();
	eraseSetBallKeys();
}

void CModelSets::calculateCollisions()
{
	for (auto &pin : mvPinPositions)
	{
		for (auto &ball : mvBallPositions)
		{
			float deltaX = ball.second.posActual[0] - pin.posActual[0];
			float deltaY = pin.posActual[1] - ball.second.posActual[1];
			float radiosAdded = ball.second.radio + pin.radio;

			if (((deltaX*deltaX) + (deltaY*deltaY)) <= (radiosAdded * radiosAdded))
			{
				float xColl =
					(pin.posActual[0] * ball.second.radio + ball.second.posActual[0] * pin.radio) / (pin.radio + ball.second.radio);
				float yColl =
					(pin.posActual[1] * ball.second.radio + ball.second.posActual[1] * pin.radio) / (pin.radio + ball.second.radio);
				float vDirX = xColl - pin.posActual[0];
				float vDirY = yColl - pin.posActual[1];
				float modVDir = sqrtf((vDirX * vDirX) + (vDirY*vDirY));

				mPhysics.ResolveBallCollision(ball.second,
				{ xColl, yColl }, { vDirX / modVDir, vDirY / modVDir });
			}
		}
	}

	for (auto &seg : mvSegmentPositions)
	{
		for (auto &ball : mvBallPositions)
		{
			phyVector2D closest = mPhysics.closestPointCircleToSegment(
			{ seg.posInit[0], seg.posInit[1] }, { seg.posEnd[0], seg.posEnd[1] },
			{ ball.second.posActual[0], ball.second.posActual[1] });
			float interX = ball.second.posActual[0] - closest.x;
			float interY = ball.second.posActual[1] - closest.y;
			float modInter = sqrtf((interX*interX) + (interY*interY));
			if (modInter < ball.second.radio)
			{
				mPhysics.ResolveBallCollision(ball.second,
				{ closest.x, closest.y }, { interX / modInter, interY / modInter });
			}
		}
	}

	for (auto &cmpBall : mvBallPositions)
	{
		for (auto &ball : mvBallPositions)
		{
			if (cmpBall.second.posActual[0] == ball.second.posActual[0] &&
				cmpBall.second.posActual[1] == ball.second.posActual[1])
				continue;

			float deltaX = ball.second.posActual[0] - cmpBall.second.posActual[0];
			float deltaY = cmpBall.second.posActual[1] - ball.second.posActual[1];
			float radiosAdded = ball.second.radio + cmpBall.second.radio;

			if (((deltaX*deltaX) + (deltaY*deltaY)) <= (radiosAdded * radiosAdded))
			{
				float xColl =
					(cmpBall.second.posActual[0] * ball.second.radio + ball.second.posActual[0] * cmpBall.second.radio) / (cmpBall.second.radio + ball.second.radio);
				float yColl =
					(cmpBall.second.posActual[1] * ball.second.radio + ball.second.posActual[1] * cmpBall.second.radio) / (cmpBall.second.radio + ball.second.radio);
				float vDirX = xColl - cmpBall.second.posActual[0];
				float vDirY = yColl - cmpBall.second.posActual[1];
				float modVDir = sqrtf((vDirX * vDirX) + (vDirY*vDirY));

				mPhysics.ResolveBallCollision(ball.second,
				{ xColl, yColl }, { vDirX / modVDir, vDirY / modVDir });
			}
		}
	}

	//auto endIterDrain = mvDrainPositions.end();
	//for (auto iterDrain = mvDrainPositions.begin(); iterDrain != endIterDrain; iterDrain++)
	int index = 0;
	for (auto &drain : mvDrainPositions)
	{
		for (auto &ball : mvBallPositions)
		{
			phyVector2D closest = mPhysics.closestPointCircleToSegment(
			{ drain.posInit[0], drain.posInit[1] }, { drain.posEnd[0], drain.posEnd[1] },
			{ ball.second.posActual[0], ball.second.posActual[1] });
			float interX = ball.second.posActual[0] - closest.x;
			float interY = ball.second.posActual[1] - closest.y;
			float modInter = sqrtf((interX*interX) + (interY*interY));
			if (modInter < ball.second.radio)
			{
				//CGUIManager::getInstance()->SendEvent(GUI_DRAIN_IMPACT, index + 1);
				//CGUIManager::getInstance()->SendEvent(GUI_BALL_IMPACT, ball.first);
			}
		}
		index++;
	}
}

void CModelSets::eraseSetBallKeys()
{
	for (auto key : mvBallKeysToDelete)
	{
		if (mvBallPositions.count(key) != 0)
			mvBallPositions.erase(key);
	}
	mvBallKeysToDelete.clear();
}


void CModelSets::Release()
{

}
