#pragma once
#include <vector>

class Process;

class CMapita {
public:
	CMapita(void){}
	~CMapita(void) {}

	void loadMapa(const std::string & level);
	void PosMapa(int x, int y);

private:
	std::vector<int> mLevel;
};