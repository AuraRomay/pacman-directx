#include <d3d11.h>
#include <cmath>
#include "Rotations.h"

void CRotationFace::setFlakesVector(FaceType type, std::vector<std::vector<CBaseFlake*>> vFlakes)
{
	switch (type)
	{
	case FaceType::ZFRONT: {
		for (int i = 0; i < 9; i++) {
			mUsedFlakes[i] = vFlakes[0][i];
		}
		for (int j = 0; j < 3; j++) {
			mUsedFlakes[9 + j] = vFlakes[4][6 + j];
			mUsedFlakes[12 + j] = vFlakes[1][3 * j];
			mUsedFlakes[15 + j] = vFlakes[5][6 + j];
			mUsedFlakes[18 + j] = vFlakes[2][3 * j];
		}
	}	break;
	case FaceType::XRIGHT: {
		for (int i = 0; i < 9; i++) {
			mUsedFlakes[i] = vFlakes[1][i];
		}
		for (int j = 0; j < 3; j++) {
			mUsedFlakes[9 + j] = vFlakes[4][8 - (3 * j)];
			mUsedFlakes[12 + j] = vFlakes[3][2 + (3 * j)];
			mUsedFlakes[15 + j] = vFlakes[5][8 - (3 * j)];
			mUsedFlakes[18 + j] = vFlakes[0][2 + (3 * j)];
		}
	}	break;
	default:
		break;
	}
}

void CRotationFace::rotateRight()
{
	for (int i = 0; i < 21; i++) {
		mSavedColors[i] = mUsedFlakes[i]->getColor();
	}
	mUsedFlakes[0]->setColor(mSavedColors[6]);
	mUsedFlakes[1]->setColor(mSavedColors[3]);
	mUsedFlakes[2]->setColor(mSavedColors[0]);
	mUsedFlakes[3]->setColor(mSavedColors[7]);
	mUsedFlakes[5]->setColor(mSavedColors[1]);
	mUsedFlakes[6]->setColor(mSavedColors[8]);
	mUsedFlakes[7]->setColor(mSavedColors[5]);
	mUsedFlakes[8]->setColor(mSavedColors[2]);

	mUsedFlakes[9]->setColor(mSavedColors[20]);
	mUsedFlakes[10]->setColor(mSavedColors[19]);
	mUsedFlakes[11]->setColor(mSavedColors[18]);

	mUsedFlakes[12]->setColor(mSavedColors[9]);
	mUsedFlakes[13]->setColor(mSavedColors[10]);
	mUsedFlakes[14]->setColor(mSavedColors[11]);

	mUsedFlakes[15]->setColor(mSavedColors[14]);
	mUsedFlakes[16]->setColor(mSavedColors[13]);
	mUsedFlakes[17]->setColor(mSavedColors[12]);

	mUsedFlakes[18]->setColor(mSavedColors[15]);
	mUsedFlakes[19]->setColor(mSavedColors[16]);
	mUsedFlakes[20]->setColor(mSavedColors[17]);
}
