#pragma once
#include <vector>
#include <memory>
#include <mutex>

struct phyVector2D
{
	float x;
	float y;
};

struct phyVector3D
{
	float x;
	float y;
	float z;
};
