#pragma once
#include <memory>
#include <list>
#include <cassert>

class Process
{
	friend class ProcessManager;

public:
	enum State
	{

		UNINITIALIZED = 0,  // created but not running
		REMOVED,
		RUNNING,
		PAUSED, 
		SUCCEEDED,
		FAILED,
		ABORTED,
	};

private:
	State m_state; 
	std::shared_ptr<Process> m_pChild; 

public:
	Process(void);
	virtual ~Process(void);

protected:
	virtual void VOnInit(void) { m_state = RUNNING; }  
	virtual void VOnUpdate(float elapsedTime) = 0; 
	virtual void VOnSuccess(void) { }
	virtual void VOnFail(void) { }
	virtual void VOnAbort(void) { }  

public:
	inline void Succeed(void);
	inline void Fail(void);
	inline void Pause(void);
	inline void UnPause(void);

	State GetState(void) const { return m_state; }
	bool IsAlive(void) const { return (m_state == RUNNING || m_state == PAUSED); }
	bool IsDead(void) const { return (m_state == SUCCEEDED || m_state == FAILED || m_state == ABORTED); }
	bool IsRemoved(void) const { return (m_state == REMOVED); }
	bool IsPaused(void) const { return m_state == PAUSED; }

	inline void AttachChild(std::shared_ptr<Process> pChild);
	std::shared_ptr<Process> RemoveChild(void); 
	std::shared_ptr<Process> PeekChild(void) { return m_pChild; } 

private:
	void SetState(State newState) { m_state = newState; }
};

inline void Process::Succeed(void)
{
	assert(m_state == RUNNING || m_state == PAUSED);
	m_state = SUCCEEDED;
}

inline void Process::Fail(void)
{
	assert(m_state == RUNNING || m_state == PAUSED);
	m_state = FAILED;
}

inline void Process::AttachChild(std::shared_ptr<Process> pChild)
{
	if (m_pChild)
		m_pChild->AttachChild(pChild);
	else
		m_pChild = pChild;
}

inline void Process::Pause(void)
{
	if (m_state == RUNNING)
		m_state = PAUSED;
}

inline void Process::UnPause(void)
{
	if (m_state == PAUSED)
		m_state = RUNNING;
}