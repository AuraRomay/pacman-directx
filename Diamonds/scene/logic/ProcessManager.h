#include "Process.h"

class ProcessManager
{
	typedef std::list<std::shared_ptr<Process>> ProcessList;

	ProcessList m_processList;

public:
	~ProcessManager(void);

	unsigned int UpdateProcesses(float elapsedTime); 
	std::shared_ptr<Process> AttachProcess(std::shared_ptr<Process> pProcess);
	void AbortAllProcesses(bool immediate);

	unsigned int GetProcessCount(void) const { return m_processList.size(); }

private:
	void ClearAllProcesses(void); 
};
