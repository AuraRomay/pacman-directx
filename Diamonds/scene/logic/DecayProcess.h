#pragma once
#include "Process.h"
#include "../model/Diamond.h"

class DecayProcess : public Process
{
public:
	explicit DecayProcess(CDiamond *diamond);

protected:
	void VOnUpdate(float elapsedTime) override;

private:
	CDiamond *mDiamond;
	float mSavedTime;
};

DecayProcess::DecayProcess(CDiamond *diamond)
	: mDiamond(diamond), mSavedTime(0.f)
{
	
}

void DecayProcess::VOnUpdate(float elapsedTime)
{
	if ((mSavedTime += elapsedTime) > 0.05f) {
		auto pos = mDiamond->getPosition();
		auto delta = mDiamond->getDeltaPosition();
		float ydelta = delta.y - 2.f;
		if ((pos.y + ydelta) < -50.f) {
			Succeed();	
		}
		mDiamond->setDeltaPosition({ delta.x, ydelta, delta.z });
		mSavedTime = 0.f;
	}
}