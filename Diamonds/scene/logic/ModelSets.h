#pragma once
#include <vector>
#include <map>
#include <memory>
#include <mutex>
#include "Physics.h"

struct ID3D11Buffer;
struct ID3D11Device;

class CModelSets
{
public:
	~CModelSets(void)		{ }
	CModelSets(void);

	static std::shared_ptr<CModelSets>& getInstance();

	void insertBall(float S0x, float S0y, float V0x, float V0y);
	void setToBeDeleted(int index);
	void eraseSetBallKeys();

	void insertPin(float S0x, float S0y);
	void insertSegment(float SAx, float SAy, float SBx, float SBy);
	void insertDrain(float SAx, float SAy, float size);
	void insertScoreNum(int number);

	void insertTexture(int id, int startIndex);
	void setTextureActive(int id, bool isActive);
	
	void insertCard(int id);
	void setCardActive(int id, bool isActive);
	void setCardAnimation(int id, int frameSize, float time);

	void setModelGravity(float g);

	void takeCardFromStack();

	std::map<int, sModelState> &getBallVectors() { return mvBallPositions; }
	std::vector<sPinState> &getPinVectors() { return mvPinPositions; }
	std::vector<int> &getScoreVectors() { return mvScorePositions; }
	std::map<int, sTextureState> &getTextureStates() { return mvTextureStates; }
	std::vector<sCardState> &getCardStates() { return mvCardStates; }
	std::vector<sCardMoveState> &getCardMoveStates() { return mvCardMoveStates; }

	void update(float deltaTime);
	void Release();

private:
	void calculateCollisions();

public:
	std::map<int, sModelState> mvBallPositions;
	std::vector<sPinState> mvPinPositions;
	std::vector<sSegmentState> mvSegmentPositions;
	std::vector<sSegmentState> mvDrainPositions;
	std::vector<int> mvScorePositions;
	std::map<int, sTextureState> mvTextureStates;
	std::vector<sCardState> mvCardStates;
	std::vector<sCardMoveState> mvCardMoveStates;

private:
	Physics mPhysics;
	std::vector<int> mvBallKeysToDelete;

	phyVector3D stackPos, deskPos[7], gravePos;
	bool deskBusy[7];

	static std::mutex _mutex;

};
