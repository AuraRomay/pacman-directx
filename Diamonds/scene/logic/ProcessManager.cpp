//========================================================================
// ProcessManager.cpp : defines common game events
// (c) Copyright 2012 Michael L. McShaffry and David Graham
//========================================================================

//#include "GameCodeStd.h"
#include "ProcessManager.h"

ProcessManager::~ProcessManager(void)
{
	ClearAllProcesses();
}

unsigned int ProcessManager::UpdateProcesses(float elapsedTime)
{
	unsigned short int successCount = 0;
	unsigned short int failCount = 0;

	ProcessList::iterator it = m_processList.begin();
	while (it != m_processList.end())
	{
		std::shared_ptr<Process> pCurrProcess = (*it);
		ProcessList::iterator thisIt = it;
		++it;

		if (pCurrProcess->GetState() == Process::UNINITIALIZED)
			pCurrProcess->VOnInit();

		if (pCurrProcess->GetState() == Process::RUNNING)
			pCurrProcess->VOnUpdate(elapsedTime);

		if (pCurrProcess->IsDead())
		{
			switch (pCurrProcess->GetState())
			{
			case Process::SUCCEEDED:
			{
				pCurrProcess->VOnSuccess();
				std::shared_ptr<Process> pChild = pCurrProcess->RemoveChild();
				if (pChild)
					AttachProcess(pChild);
				else
					++successCount;
				break;
			}

			case Process::FAILED:
			{
				pCurrProcess->VOnFail();
				++failCount;
				break;
			}

			case Process::ABORTED:
			{
				pCurrProcess->VOnAbort();
				++failCount;
				break;
			}
			}
			m_processList.erase(thisIt);
		}
	}

	return ((successCount << 16) | failCount);
}

std::shared_ptr<Process> ProcessManager::AttachProcess(std::shared_ptr<Process> pProcess)
{
	m_processList.push_front(pProcess);
	return pProcess;
}

void ProcessManager::ClearAllProcesses(void)
{
	m_processList.clear();
}

void ProcessManager::AbortAllProcesses(bool immediate)
{
	ProcessList::iterator it = m_processList.begin();
	while (it != m_processList.end())
	{
		ProcessList::iterator tempIt = it;
		++it;

		std::shared_ptr<Process> pProcess = *tempIt;
		if (pProcess->IsAlive())
		{
			pProcess->SetState(Process::ABORTED);
			if (immediate)
			{
				pProcess->VOnAbort();
				m_processList.erase(tempIt);
			}
		}
	}
}


