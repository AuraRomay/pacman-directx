//========================================================================
// Process.cpp : defines common game events
// (c) Copyright 2012 Michael L. McShaffry and David Graham
//========================================================================

//#include "GameCodeStd.h"
#include "Process.h"

Process::Process(void)
{
	m_state = UNINITIALIZED;
	//m_pParent = NULL;
	//m_pChild = NULL;
}

Process::~Process(void)
{
	if (m_pChild)
	{
		m_pChild->VOnAbort();
	}
}

std::shared_ptr<Process> Process::RemoveChild(void)
{
	if (m_pChild)
	{
		std::shared_ptr<Process> pChild = m_pChild;
		m_pChild.reset();
		//pChild->SetParent(NULL);
		return pChild;
	}

	return std::shared_ptr<Process>();
}

