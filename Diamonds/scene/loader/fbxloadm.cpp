#include <fbxsdk.h>
#include <vector>
#include <cassert>

FbxManager* g_pFbxSdkManager = nullptr;

bool LoadFBX()//std::vector* pOutVertexVector)
{
	if (g_pFbxSdkManager == nullptr)
	{
		g_pFbxSdkManager = FbxManager::Create();

		FbxIOSettings* pIOsettings = FbxIOSettings::Create(g_pFbxSdkManager, IOSROOT);
		g_pFbxSdkManager->SetIOSettings(pIOsettings);
	}

	FbxImporter* pImporter = FbxImporter::Create(g_pFbxSdkManager, "");
	FbxScene* pFbxScene = FbxScene::Create(g_pFbxSdkManager, "");

	bool bSuccess = pImporter->Initialize("data/rham.fbx", -1, g_pFbxSdkManager->GetIOSettings());
	if (!bSuccess) return false;

	bSuccess = pImporter->Import(pFbxScene);
	if (!bSuccess) return false;

	pImporter->Destroy();

	FbxNode* pFbxRootNode = pFbxScene->GetRootNode();

	if (pFbxRootNode)
	{
		for (int i = 0; i < pFbxRootNode->GetChildCount(); i++)
		{
			FbxNode* pFbxChildNode = pFbxRootNode->GetChild(i);

			if (pFbxChildNode->GetNodeAttribute() == NULL) {
				//continue;
				bool needContinue = true;
				for (int j = 0; j < pFbxChildNode->GetChildCount(); j++) {
					FbxNode* pFbxGrandSonNode = pFbxChildNode->GetChild(j);
					if(pFbxChildNode->GetNodeAttribute() == NULL) {
						pFbxChildNode = pFbxGrandSonNode;
						needContinue = false;
						break;
					}
				}
				if (needContinue) { continue; }
			}

			FbxNodeAttribute::EType AttributeType = pFbxChildNode->GetNodeAttribute()->GetAttributeType();

			if (AttributeType != FbxNodeAttribute::eMesh)
				continue;

			FbxMesh* pMesh = (FbxMesh*)pFbxChildNode->GetNodeAttribute();

			FbxVector4* pVertices = pMesh->GetControlPoints();
			int pcoi = pMesh->GetPolygonCount();

			FbxStringList lUVNames;
			pMesh->GetUVSetNames(lUVNames);
			const char * lUVName = NULL;
			if (lUVNames.GetCount()) {				
				lUVName = lUVNames[0];
			}

			for (int j = 0; j < pMesh->GetPolygonCount(); j++)
			{
				int iNumVertices = pMesh->GetPolygonSize(j);
				assert(iNumVertices == 3);

				for (int k = 0; k < iNumVertices; k++) {
					int iControlPointIndex = pMesh->GetPolygonVertex(j, k);

					

					//MyVertex vertex;
					//vertex.pos[0] = (float)pVertices[iControlPointIndex].mData[0];
					//vertex.pos[1] = (float)pVertices[iControlPointIndex].mData[1];
					//vertex.pos[2] = (float)pVertices[iControlPointIndex].mData[2];
					float a = (float)pVertices[iControlPointIndex].mData[0];
					float b = (float)pVertices[iControlPointIndex].mData[1];
					float c = (float)pVertices[iControlPointIndex].mData[2];
					//pOutVertexVector->push_back(vertex);
					FbxVector4 fbxNormal;
					pMesh->GetPolygonVertexNormal(j, k, fbxNormal);
					fbxNormal.Normalize();
					float na = (float)fbxNormal[0];
					float nb = (float)fbxNormal[1];
					float nc = (float)fbxNormal[2];

					bool lUnmappedUV;
					FbxVector2 fbxTexCords;
					pMesh->GetPolygonVertexUV(j, k, lUVName, fbxTexCords, lUnmappedUV);
					float ta = (float)fbxTexCords[0];
					float tb = (float)fbxTexCords[1];

				}
			}

		}

	}
	return true;
}