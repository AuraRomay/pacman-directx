#include <d3d11.h>

#include <string>
#include "SceneMgr.h"
#include "../render/shaders/ShaderColor.h"
#include "../render/shaders/ShaderTexture.h"
#include "../render/shaders/ShaderTexture2.h"
#include "model/IronGrid.h"
#include "model/Diamond.h"
#include "model/CuboOne.h"
#include "model/CuboOne2.h"
#include "logic/DecayProcess.h"

using namespace std;

static float cblack[3] = { 0.0f, 0.0f, 0.0f };
static float cblue[3] = { 0.01176f, 0.01568f, 0.43529f };

bool LoadFBX();

CSceneMgr::CSceneMgr(void)
: mpDevice(nullptr)
, mpDcontext(nullptr)
, mShaderColor(nullptr)
, mShaderTexture(nullptr)
{	
}

CSceneMgr::~CSceneMgr(void)
{
	//CIronGrid::getInstance()->Release();
	//DXCamera::getInstance()->Release();

	//for each (auto &diam in mDiamantes) {
	//	diam->Release();
	//	delete diam;
	//}

	if (mShaderColor != nullptr) {
		delete mShaderColor;
	}
	if (mShaderTexture != nullptr) {
		delete mShaderTexture;
	}
}

void CSceneMgr::Initialize(
	ID3D11Device *device, ID3D11DeviceContext *dcontext)
{
	mpDevice = device;
	mpDcontext = dcontext;
	
	//DXCamera::getInstance()->SetCamera(0.f, 0.f, -70.f);
	DXCamera::getInstance()->SetCamera(0.f, 0.f, -70.f);
	//CIronGrid::getInstance()->Initialize(device, dcontext);
	//CIronGrid::getInstance()->InsertModel();

	mShaderColor = new ShaderColor(device, dcontext);
	mShaderColor->Initialize("ColorShader",
		posVertexLayoutDesc, ARRAYSIZE(posVertexLayoutDesc));

	mShaderTexture = new ShaderTexture(device, dcontext);
	mShaderTexture->Initialize("textureShader",
		textureVertexLayoutDesc, ARRAYSIZE(textureVertexLayoutDesc));

	mShaderTexture2 = new ShaderTexture2(device, dcontext);
	mShaderTexture2->Initialize("textureShader2",
		textureVertexLayoutDesc2, ARRAYSIZE(textureVertexLayoutDesc2));

	auto diam = new CDiamond();
	diam->Initialize(mpDevice, mpDcontext);
	diam->InsertModel(0, 10.f, 0);
	diam->setColor({ 0.8f, 0.8f, 0.1f });

	auto irong = new CIronGrid();
	irong->Initialize(mpDevice, mpDcontext);
	irong->InsertModel();
	irong->setColor({ 0.72f, 0.72f, 0.72f });

	/*CCuboOne *cubin = new CCuboOne();
	cubin->Initialize(mpDevice, mpDcontext);
	cubin->InsertModel("data/pacman_abierto.obj", "data/pacman_cerrado.obj");
	cubin->setPosition({ 0, 0, 0 });
	mShaderTexture->insertObjectToShader(cubin);
	mCubines.push_back(cubin);*/

	/*modelo.push_back(new CCuboOne2());
	auto cubin2 = &*modelo.back();
	phyVector3D pl{ 1, 1, 0 };
	phyVector3D pl2{ 20, 20, 1 };
	cubin2->Initialize(mpDevice, mpDcontext);
	cubin2->InsertModel("data/MazeFin.obj", pl, pl2, 1);
	mShaderTexture2->insertObjectToShader(cubin2);*/
	crearPac(0.f, 0.f, 0.f, 0);

	crearFac(9.f, 0.f, 0.f, 2);
	crearFac(14.f, -15.f, 0.f, 1);
	crearFac(-12.f, 10.f, 0.f, 3);
	crearFac(-6.f, -20.f, 0.f, 4);
	
	crearMaze(posI, Mesc, 1, "data/MazeFin.obj");
	crearMaze(posI, Mesc, 2, "data/ballsDown.obj");
	crearMaze(posI, Mesc, 2, "data/Ballsup.obj");

	mapa.loadMapa("data/level.txt");
	//mShaderColor->insertObjectToShader(diam);
	//mShaderColor->insertObjectToShader(irong);

	//mShaderTexture->insertObjectToShader(mCubin);

	//LoadFBX();
}

void CSceneMgr::crearPac(float x, float y, float z, int ind)
{
	CCuboOne *cubin = new CCuboOne();
	cubin->Initialize(mpDevice, mpDcontext);
	cubin->InsertModel("data/pacman_abierto.obj", "data/pacman_cerrado.obj", ind);
	cubin->setPosition({ x, y, z });
	mShaderTexture->insertObjectToShader(cubin);
	mCubines.push_back(cubin);
}

void CSceneMgr::crearFac(float x, float y, float z, int ind)
{
	CCuboOne *fan = new CCuboOne();
	fan->Initialize(mpDevice, mpDcontext);
	fan->InsertModel("data/Ghost.obj", "data/Ghost.obj", ind);
	fan->setPosition({ x, y, z });
	mShaderTexture->insertObjectToShader(fan);
	mFan.push_back(fan);
}

/*void CSceneMgr::crearFantasmas(phyVector3D pos, phyVector3D esc, int ind)
{
	CCuboOne2 *fant = new CCuboOne2();
	phyVector3D pl1{ 1, 1, 0 };
	phyVector3D pl21{ 1, 1, 1 };
	fant->Initialize(mpDevice, mpDcontext);
	fant->InsertModel("data/Ghost.obj", pos, esc, ind);
	mShaderTexture2->insertObjectToShader(fant);
	mFantasmas.push_back(fant);
}*/

void CSceneMgr::crearMaze(phyVector3D pos, phyVector3D esc, int ind, std::string name)
{
	CCuboOne2 *maze = new CCuboOne2();
	maze->Initialize(mpDevice, mpDcontext);
	maze->InsertModel(name, pos, esc, ind);
	mShaderTexture2->insertObjectToShader(maze);
	mMaze.push_back(maze);
}

float CSceneMgr::ChangeCamera(bool ver)
{
	if (ver == false) {
		cx = mCubines.front()->GetPosition().x;
		cy = mCubines.front()->GetPosition().y - 1.f;
		cz = mCubines.front()->GetPosition().z - 10.f;
		DXCamera::getInstance()->SetCamera(cx, cy, cz);
		return cx, cy, cz;
	}
	if (ver == true) {
		cx = 0.f;
		cy = 0.f;
		cz = -70.f;
		DXCamera::getInstance()->SetCamera(cx, cy, cz);
		return cx, cy, cz;
	}
}

void CSceneMgr::ChangeModelRotation(float head, float pitch)
{
	for (auto cubin : mCubines) {
		cubin->setRotateAngles({ head, pitch, 0.0f });
	}
}

void CSceneMgr::GetFP() {
	std::vector<CCuboOne*>::iterator it = mFan.begin();
	for (it; it < mFan.end(); it++) {
		(*it)->GetPosition().x;
		(*it)->GetPosition().y;
	}
}

void CSceneMgr::ChangeFirstModelPos(float x, float y, int index)
{
	mCubines[index]->deltaPosition({ x,y,0 });
	//GetFP();
}

void CSceneMgr::ChangePos(float x, float y, int index)
{
	mFan[index]->deltaPosition({ x,y,0 });
}

void CSceneMgr::CheckFP() {
	std::vector<CCuboOne*>::iterator it = mFan.begin();
	for (it; it < mFan.end(); it++) {
		if ((*it)->GetPosition().x - 1 <= mCubines.front()->GetPosition().x && (*it)->GetPosition().x + 1 <= mCubines.front()->GetPosition().x && (*it)->GetPosition().y-1 <= mCubines.front()->GetPosition().y && (*it)->GetPosition().y + 1 >= mCubines.front()->GetPosition().y) {
			mCubines.front()->setPosition({ 0.f, 0.f, 0.f });
		}
	}
}

void CSceneMgr::update(float deltaTime)
{

		/*static float acreTime = 0.f;
		if ((acreTime += deltaTime) > 1.f) {
			float mx = static_cast<float>(rand() % 100 - 50);
			float my = static_cast<float>(rand() % 100 - 50);
			float mz = static_cast<float>(rand() % 100 - 50);
			auto diam = new CDiamond();
			diam->Initialize(mpDevice, mpDcontext);
			diam->InsertModel(mx, 70.f, mz);
			diam->setColor({ 0.8f, 0.8f, 0.1f });
			mDiamantes.push_back(diam);
	
			auto pDecay = std::make_shared<DecayProcess>(diam);
			mProcessManager.AttachProcess(pDecay);
			acreTime = 0.f;
		}
		mProcessManager.UpdateProcesses(deltaTime);*/

	ChangeCamera(cam);
	GetFP();
	CheckFP();
}

void CSceneMgr::draw()
{
	//CIronGrid::getInstance()->drawModel();
	mShaderColor->Update();
	mShaderTexture->Update();
	mShaderTexture2->Update();
	//for each (auto &diam in mDiamantes) {
	//	diam->drawModel();
	//}
}
