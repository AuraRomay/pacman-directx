#ifndef SCENEMGR_H
#define SCENEMGR_H

#include <vector>
#include <string>
//#include "../render/shaders/ShaderColor.h"
#include "../render/DXCamera.h"
#include "logic/ProcessManager.h"
#include "logic/Mapita.h"

struct ID3D11Device;
class ShaderColor;
class ShaderTexture;
class ShaderTexture2;
class CDiamond;
class CCuboOne;
class CCuboOne2;

class CSceneMgr
{
private:

public:
	CSceneMgr(void);
	~CSceneMgr(void);

	void Initialize(ID3D11Device *device, ID3D11DeviceContext *dcontext);
	void crearPac(float x, float y, float z, int ind);
	void crearFac(float x, float y, float z, int ind);
	//void crearFantasmas(phyVector3D pos, phyVector3D esc, int ind);
	void crearMaze(phyVector3D pos, phyVector3D esc, int ind, std::string name);
	void update(float deltaTime);

	void draw();
	float ChangeCamera(bool ver);
	/*bool Camare(bool )*/
	void ChangeModelRotation(float head, float pitch);
	void GetFP();

	void ChangeFirstModelPos(float x, float y, int index);
	void ChangePos(float x, float y, int index);
	void CheckFP();

	bool cam = false;


private:
	//ShaderBase mpSimpleShader;
	ShaderColor *mShaderColor;
	ShaderTexture *mShaderTexture;
	ShaderTexture2 *mShaderTexture2;


	ProcessManager mProcessManager;

	float mClearColor[4];

	ID3D11Device *mpDevice;
	ID3D11DeviceContext *mpDcontext;
	//std::list<CDiamond*> mDiamantes;
	std::vector<CCuboOne *> mCubines;
	std::vector<CCuboOne *> mFan;
	//std::vector<CCuboOne2*> mFantasmas;
	std::vector<CCuboOne2*> mMaze;

	float cx = 0.f;
	float cy = 0.f;
	float cz = 0.f;

	phyVector3D posI{ 1.f, 1.f, 1.f };
	phyVector3D f1{ 1.f, 1.f, 1.f };
	phyVector3D f2{ 5.f, 4.f, 1.f };
	phyVector3D f3{ 7.f, 10.f, 1.f };
	phyVector3D f4{ -3.f, -2.f, 1.f };

	phyVector3D esc{ 1.f,1.f,1.f };
	phyVector3D Mesc{ 20.f,20.f,20.f };

	CMapita mapa;
};

#endif
