#include <d3d11.h>
#include <new>
#include "../Constants.h"
#include "Pin.h"

std::mutex CPin::_mutex;
static CPin *instance = nullptr;

CPin *CPin::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)		
			instance = new CPin();		
	}
	return instance;
}

CPin::CPin(void)
: CBasePosModel()
{
}

void CPin::InsertModel(float xOffset, float yOffset)
{
	const float xyScale = RADIOPIN, zScale = 2.0f;
	const float angleDivided = 0.5236f / 8.f;

	std::vector<BasicPosVertex> vertices;
	std::vector<unsigned short> indices;

	vertices.push_back({ { xOffset, yOffset, 0.0f } });
	for (int i = 1; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			float x = (sinf(angleDivided*i) * cosf(0.7854f*j)) + xOffset;
			float y = (sinf(angleDivided*i) * sinf(0.7854f*j)) + yOffset;
			float z = 1.f-cosf(angleDivided*i);
			vertices.push_back({ { x, y, z } });
		}
	}

	for (unsigned short j = 0; j < 7; j++)
	{
		indices.push_back(0);
		indices.push_back(j + 2);
		indices.push_back(j + 1);
	}
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(8);

	for (unsigned short i = 1; i < 42; i += 8)
	{
		for (unsigned short j = 0; j < 7; j++)
		{
			indices.push_back(j + i);
			indices.push_back(j + i + 1);
			indices.push_back(j + i + 8);

			indices.push_back(j + i + 1);
			indices.push_back(j + i + 9);
			indices.push_back(j + i + 8);
		}
		indices.push_back(i + 7);
		indices.push_back(i);
		indices.push_back(i + 15);

		indices.push_back(i);
		indices.push_back(i + 8);
		indices.push_back(i + 15);
	}

	for (unsigned short i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	vertices.clear();
	indices.clear();

	const float cylinderZPushBack = 0.05f;
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			float x = (cosf(0.7854f*j) * xyScale) + xOffset;
			float y = (sinf(0.7854f*j) * xyScale) + yOffset;
			float z = ((float) i + cylinderZPushBack) * zScale;
			vertices.push_back({ { x, y, z } });
		}
	}

	for (unsigned short j = 0; j < 7; j++)
	{
		indices.push_back(j);
		indices.push_back(j + 1);
		indices.push_back(j + 8);

		indices.push_back(j + 1);
		indices.push_back(j + 9);
		indices.push_back(j + 8);
	}
	indices.push_back(7);
	indices.push_back(0);
	indices.push_back(15);

	indices.push_back(0);
	indices.push_back(8);
	indices.push_back(15);

	for (unsigned short i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

void CPin::Release()
{
	CBasePosModel::Release();
	delete instance;
}

//TextureBase *CPin::getTexture()
//{
//	return CTextureMgr::getInstance()->getTexture("pin");
//}
