#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BaseTextureModel.h"

struct ID3D11Buffer;
struct ID3D11Device;

class CPanel : public CBaseTextureModel
{
public:
	~CPanel(void)		{ }

	static CPanel *getInstance();
	unsigned short InsertModel(float x, float y, float z, float wp, float hp,
		float u, float v, float wt, float ht);
	
	void drawModel() override;
	TextureBase *getTexture() override;
	void Release() override;

private:
	CPanel(void);

private:
	static std::mutex _mutex;

};
