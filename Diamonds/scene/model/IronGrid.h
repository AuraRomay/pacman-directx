#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BasePosModel.h"

class CIronGrid : public CBasePosModel
{
public:
	CIronGrid(void) : CBasePosModel() { }
	~CIronGrid(void)		{ }

	//static CIronGrid *getInstance();
	void InsertModel();
	void drawModel() override;
	//phyVector3D getColor() override { return{ 0.72f, 0.72f, 0.72f }; }

	//void Release() override;

//private:

//private:
	//static std::mutex _mutex;

};
