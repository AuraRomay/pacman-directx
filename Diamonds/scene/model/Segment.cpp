#include <d3d11.h>
#include <cmath>
#include <new>
#include "../Constants.h"
#include "Segment.h"

std::mutex CSegment::_mutex;
static CSegment *instance = nullptr;

CSegment *CSegment::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)
			instance = new CSegment();
	}
	return instance;
}

CSegment::CSegment(void)
: CBasePosModel()
{
}

void CSegment::InsertModel(float x0, float y0, float x1, float y1)
{
	const float thickness = 0.2f;
	const float height = 1.0f;
	float angtan = atanf((y1 - y0) / (x1 - x0));
	float xOrtogonal = -sinf(angtan);
	float yOrtogonal = cosf(angtan);
	float xUpLeft = thickness * xOrtogonal + x0;
	float yUpLeft = thickness * yOrtogonal + y0;
	float xDnLeft = thickness * -xOrtogonal + x0;
	float yDnLeft = thickness * -yOrtogonal + y0;

	float xUpRight = thickness * xOrtogonal + x1;
	float yUpRight = thickness * yOrtogonal + y1;
	float xDnRight = thickness * -xOrtogonal + x1;
	float yDnRight = thickness * -yOrtogonal + y1;

	std::vector<BasicPosVertex> vertices {
		{ { xUpLeft, yUpLeft, height } },
		{ { xDnLeft, yDnLeft, height } },
		{ { xUpRight, yUpRight, height } },
		{ { xDnRight, yDnRight, height } },

		{ { xUpLeft, yUpLeft, 0.f } },
		{ { xDnLeft, yDnLeft, 0.f } },
		{ { xUpRight, yUpRight, 0.f } },
		{ { xDnRight, yDnRight, 0.f } }
	};

	std::vector<unsigned short> indices{
		0, 2, 3, 0, 3, 1,
		1, 3, 5, 3, 7, 5,
		4, 7, 6, 4, 5, 7,
		2, 0, 4, 2, 4, 6,
		0, 1, 5, 0, 5, 4,
		3, 2, 6, 3, 6, 7,
	};

	for (unsigned short i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

void CSegment::Release()
{
	CBasePosModel::Release();
	delete instance;
}
