#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "VertexDefs.h"
//#include "../../render/shaders/ShaderColor.h"
#include "../logic/UMath.h"
class ShaderTexture;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Buffer;

class CTextureModel
{
public:
	~CTextureModel(void)		{ }

	void Initialize(ID3D11Device *device, ID3D11DeviceContext *context);

	ID3D11Buffer *GetVertices()			{ return mpVertexBuffer; }
	ID3D11Buffer *GetIndices()			{ return mpIndexBuffer; }
	unsigned int GetIndexBufferSize()	{ return mIndexBufferSize; }

	virtual void drawModel();

	virtual void setRotateAngles(phyVector3D rotang)	{ mRotateAngles = rotang; }
	virtual void setPosition(phyVector3D pos) { mPosition = pos; }
	virtual void deltaPosition(phyVector3D pos) { 
		mPosition.x += pos.x;
		mPosition.y += pos.y;
		mPosition.z += pos.z;
	}

	virtual void Release();

	void setRenderProperties(ShaderTexture *shader);


	phyVector3D GetPosition()
	{
		return mPosition;
	}

protected:
	CTextureModel(void);
	void SetBuffers();
	void activateBuffers();
	//void activateLineBuffers();

protected:
	//ShaderColor mpColorShader;

	std::vector<KeyframeVertex> mAllModelsVertices;
	std::vector<unsigned short> mAllModelsIndices;

	ID3D11Buffer *mpVertexBuffer;
	ID3D11Buffer *mpIndexBuffer;
	ID3D11Device *mpDevice;
	ID3D11DeviceContext *mpContext;
	unsigned int mIndexBufferSize;
	int ind;

private:
	phyVector3D mRotateAngles;
	phyVector3D mPosition;
};
