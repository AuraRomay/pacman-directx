#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "BasePosModel.h"
#include "../../render/shaders/ShaderColor.h"
#include "../../render/DXCamera.h"

CBasePosModel::CBasePosModel(void)
: mpVertexBuffer(nullptr), mpIndexBuffer(nullptr)
, mIndexBufferSize(0), mpDevice(nullptr)
{
}

void CBasePosModel::Initialize(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mpDevice = device;
	mpContext = context;

	//mpColorShader.Initialize("ColorShader",
	//	posVertexLayoutDesc, ARRAYSIZE(posVertexLayoutDesc), device);
}

void CBasePosModel::SetBuffers()
{
	if (mpDevice == nullptr)
		return;

	mIndexBufferSize = mAllModelsIndices.size();

	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(BasicPosVertex) * mAllModelsVertices.size();
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = &mAllModelsVertices[0];
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	mpDevice->CreateBuffer(
		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);

	if (mAllModelsIndices.size() > 0)
	{
		D3D11_BUFFER_DESC indexBufferDesc;
		indexBufferDesc.ByteWidth = sizeof(unsigned short) * mAllModelsIndices.size();
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexBufferData;
		indexBufferData.pSysMem = &mAllModelsIndices[0];
		indexBufferData.SysMemPitch = 0;
		indexBufferData.SysMemSlicePitch = 0;

		mpDevice->CreateBuffer(
			&indexBufferDesc, &indexBufferData, &mpIndexBuffer);
	}
}

void CBasePosModel::drawModel()
{
	//setRenderProperties();
	activateBuffers();
	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
}

void CBasePosModel::activateBuffers()
{
	unsigned int stride = sizeof(BasicPosVertex);
	unsigned int offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
	mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void CBasePosModel::activateLineBuffers()
{
	unsigned int stride = sizeof(BasicPosVertex);
	unsigned int offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
	//mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
}

void CBasePosModel::setRenderProperties(ShaderColor *shader)
{

	shader->updateShaderVariables(mColor);


	//ID3D11VertexShader *vshader = mpColorShader.GetVertexShader();
	//ID3D11PixelShader *pshader = mpColorShader.GetPixelShader();
	//mpContext->VSSetShader(vshader, nullptr, 0);
	//mpContext->PSSetShader(pshader, nullptr, 0);

	//ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();

	//MathUtil::CMatrix nview = DXCamera::getInstance()->GetView();
	//MathUtil::CMatrix nproj = DXCamera::getInstance()->GetProjection();
	//
	//bufvdat.view = MathUtil::MatrixTranspose(nview);
	//bufvdat.projection = MathUtil::MatrixTranspose(nproj);

	//mpContext->UpdateSubresource(
	//	conbuf, 0, nullptr, &bufvdat, 0, 0);
	//mpContext->VSSetConstantBuffers(0, 1, &conbuf);
	////---
	//ID3D11Buffer *conbuf;
	//conbuf = mpColorShader.GetPConstantBuffer();
	//sPConstantBuffer &bufpdat = mpColorShader.GetPConstantBufferData();
	//phyVector3D color = getColor();
	//bufpdat.color[0] = color.x;
	//bufpdat.color[1] = color.y;
	//bufpdat.color[2] = color.z;
	//bufpdat.color[3] = 1.f;

	//mpContext->UpdateSubresource(
	//	conbuf, 0, nullptr, &bufpdat, 0, 0);
	//mpContext->PSSetConstantBuffers(0, 1, &conbuf);

	//ID3D11InputLayout *inlay = mpColorShader.GetInputLayout();
	//mpContext->IASetInputLayout(inlay);
}


void CBasePosModel::Release()
{
	if (mpVertexBuffer != nullptr)
	{
		mpVertexBuffer->Release();
		mpVertexBuffer = nullptr;
	}
	if (mpIndexBuffer != nullptr)
	{
		mpIndexBuffer->Release();
		mpIndexBuffer = nullptr;
	}
}
