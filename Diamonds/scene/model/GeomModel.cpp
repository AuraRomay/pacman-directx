#include <d3d11.h>

#include "GeomModel.h"

CGeomModel::CGeomModel(void)
	: mpVertexBuffer(nullptr)
{
}

void CGeomModel::Initialize(ID3D11Device *device)
{
	//BasicVertex cuadroVertices[] = {
	//	{ {-0.5f, -0.5f, 0.f}, {0.0f, 1.0f} },
	//	{ { 0.5f, -0.5f, 0.f}, {1.0f, 1.0f} },
	//	{ { 0.5f,  0.5f, 0.f}, {1.0f, 0.0f} },
	//	{ {-0.5f,  0.5f, 0.f}, {0.0f, 0.0f} }
	//};

	//unsigned short cuadroIndices[] = {
	//	0, 2, 1,
	//	0, 3, 2
	//};

	//BasicVertex cuadroVertices[] = {
	//	{ {-0.5f, -0.5f, 0.f}, {0.0f, 0.0f, 1.0f, 1.0f} },
	//	{ { 0.5f, -0.5f, 0.f}, {0.0f, 1.0f, 0.0f, 1.0f} },
	//	{ { 0.5f,  0.5f, 0.f}, {0.0f, 1.0f, 1.0f, 1.0f} },
	//	{ {-0.5f,  0.5f, 0.f}, {1.0f, 0.0f, 0.0f, 1.0f} }
	//};

	//unsigned short cuadroIndices[] = {
	//	0, 2, 1,
	//	0, 3, 2
	//};

	//BasicVertex cuadroVertices[] = {
	//	{ {-0.5f,  0.0f, -0.5f}, {0.0f, 0.0f, 1.0f, 1.0f} },
	//	{ { 0.5f,  0.0f, -0.5f}, {0.0f, 1.0f, 0.0f, 1.0f} },
	//	{ { 0.5f,  0.0f,  0.5f}, {0.0f, 1.0f, 1.0f, 1.0f} },
	//	{ {-0.5f,  0.0f,  0.5f}, {1.0f, 0.0f, 0.0f, 1.0f} },
	//	{ { 0.0f,  1.0f,  0.0f}, {1.0f, 1.0f, 0.0f, 1.0f} },
	//	{ { 0.0f, -1.0f,  0.0f}, {1.0f, 0.0f, 1.0f, 1.0f} }
	//};

	//unsigned short cuadroIndices[] = {
	//	0, 4, 1,
	//	1, 4, 2,
	//	2, 4, 3,
	//	3, 4, 0,
	//	0, 1, 5,
	//	1, 2, 5,
	//	2, 3, 5,
	//	3, 0, 5
	//};

	GeomVertex cuadroVertices[] = {
		{ {0.75f, 0.75f, 0.5f}, {0.15f} },
	};

	//unsigned short cuadroIndices[] = {
	//	0, 4, 1,
	//	1, 4, 2,
	//	2, 4, 3,
	//	3, 4, 0,
	//	0, 1, 5,
	//	1, 2, 5,
	//	2, 3, 5,
	//	3, 0, 5
	//};

	//mIndexBufferSize = ARRAYSIZE(cuadroIndices);

	D3D11_BUFFER_DESC vertexBufferDesc = {0};
	vertexBufferDesc.ByteWidth = sizeof(GeomVertex) * ARRAYSIZE(cuadroVertices);
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = cuadroVertices;
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;
	
	device->CreateBuffer(
		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);
		

	//D3D11_BUFFER_DESC indexBufferDesc;
	//indexBufferDesc.ByteWidth = sizeof(unsigned short) * ARRAYSIZE(cuadroIndices);
	//indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	//indexBufferDesc.CPUAccessFlags = 0;
	//indexBufferDesc.MiscFlags = 0;
	//indexBufferDesc.StructureByteStride = 0;

	//D3D11_SUBRESOURCE_DATA indexBufferData;
	//indexBufferData.pSysMem = cuadroIndices;
	//indexBufferData.SysMemPitch = 0;
	//indexBufferData.SysMemSlicePitch = 0;

	//device->CreateBuffer(
	//	&indexBufferDesc, &indexBufferData, &mpIndexBuffer );
}

void CGeomModel::Release()
{
	mpVertexBuffer->Release();
}

	//BasicVertex cuadroVertices[] = {
	//	{ {-0.5f,  0.0f, -0.5f}, {0.0f, 0.0f, 1.0f, 1.0f} },
	//	{ { 0.5f,  0.0f, -0.5f}, {0.0f, 1.0f, 0.0f, 1.0f} },
	//	{ { 0.5f,  0.0f,  0.5f}, {0.0f, 1.0f, 1.0f, 1.0f} },
	//	{ {-0.5f,  0.0f,  0.5f}, {1.0f, 0.0f, 0.0f, 1.0f} },
	//	{ { 0.0f,  1.0f,  0.0f}, {1.0f, 1.0f, 0.0f, 1.0f} },
	//	{ { 0.0f, -1.0f,  0.0f}, {1.0f, 0.0f, 1.0f, 1.0f} }
	//};

	//unsigned short cuadroIndices[] = {
	//	0, 4, 1,
	//	1, 4, 2,
	//	2, 4, 3,
	//	3, 4, 0,
	//	0, 1, 5,
	//	1, 2, 5,
	//	2, 3, 5,
	//	3, 0, 5
	//};