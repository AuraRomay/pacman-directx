#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BaseTextureModel.h"

class CScore : public CBaseTextureModel
{
public:
	~CScore(void)		{ }

	static CScore *getInstance();
	void InsertModel(float x0, float y0, float x1, float y1);
	float getXOffset(int relativePosition);
	unsigned int getIndexForNumber(int number);

	void drawModel() override;
	TextureBase *getTexture() override;
	void Release() override;

private:
	CScore(void);

private:
	float mXAbsOffset;
	static std::mutex _mutex;

};
