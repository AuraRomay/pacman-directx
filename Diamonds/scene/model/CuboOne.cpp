#include <d3d11.h>
#include <cmath>
#include <new>
#include <iostream>
#include "CuboOne.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include "../loader/tiny_obj_loader.h"

void CCuboOne::InsertModel(const std::string &primer, const std::string &second, int indc)
{
	ind = indc;

	if (mpDevice == nullptr)
		return;

	auto mainVertex = loadModelToVertices(primer);// , vertices);
	auto secondVertex = loadModelToVertices(second);// , vertices);

	std::vector<KeyframeVertex> compositeVertex;
	for (size_t i = 0; i < mainVertex.size(); i++) {
		KeyframeVertex cpver = {
			{ mainVertex[i].pos[0], mainVertex[i].pos[1], mainVertex[i].pos[2], 1.f },
			{ mainVertex[i].normal[0], mainVertex[i].normal[1], mainVertex[i].normal[2] },
			{ mainVertex[i].texCoord[0], mainVertex[i].texCoord[1] },
			{ secondVertex[i].pos[0], secondVertex[i].pos[1], secondVertex[i].pos[2] },
			{ secondVertex[i].normal[0], secondVertex[i].normal[1], secondVertex[i].normal[2] }
		};		
		compositeVertex.push_back(cpver);
	}

	mAllModelsVertices.insert(mAllModelsVertices.end(), compositeVertex.begin(), compositeVertex.end());

	std::vector<unsigned short> indices;
	for (size_t i = 0; i < mainVertex.size(); i++) {
		indices.push_back(i);
	}
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

std::vector<TextureVertex> CCuboOne::loadModelToVertices(const std::string & modelname)//, std::vector<TextureVertex> &vertices)
{
	std::vector<TextureVertex> vertices;
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, modelname.c_str());

	if (!err.empty()) { std::cerr << err << std::endl; }
	if (!ret)					{	exit(1); }

	for (size_t s = 0; s < shapes.size(); s++) 
	{
		size_t index_offset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) 
		{
			int fv = shapes[s].mesh.num_face_vertices[f];
			for (size_t v = 0; v < fv; v++) 
			{				
				tinyobj::index_t idx = shapes[s].mesh.indices[index_offset + v];
				TextureVertex allv;
				allv.pos[0] = attrib.vertices[3 * idx.vertex_index + 0];
				allv.pos[1] = attrib.vertices[3 * idx.vertex_index + 1];
				allv.pos[2] = attrib.vertices[3 * idx.vertex_index + 2];

				allv.normal[0] = attrib.normals[3 * idx.normal_index + 0];
				allv.normal[1] = attrib.normals[3 * idx.normal_index + 1];
				allv.normal[2] = attrib.normals[3 * idx.normal_index + 2];

				allv.texCoord[0] = attrib.texcoords[2 * idx.texcoord_index + 0];
				allv.texCoord[1] = attrib.texcoords[2 * idx.texcoord_index + 1];
				vertices.push_back(allv);
			}
			index_offset += fv;
		}
	}
	return vertices;
}

//void CDiamond::drawModel()
//{
//	//setRenderProperties();
//	activateBuffers();
//	//ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
//	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();
//
//	//MathUtil::CMatrix translation =
//	//	MathUtil::MatrixTranslation(0.0f, mDeltaPosition.y, 0.0f);
//	//bufvdat.model = MathUtil::MatrixTranspose(translation);
//	//mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
//	//mpContext->VSSetConstantBuffers(0, 1, &conbuf);
//
//	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
//
//}