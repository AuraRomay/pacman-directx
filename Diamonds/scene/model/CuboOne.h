#pragma once
//#include <vector>
//#include <memory>
//#include <mutex>
#include <string>
#include "TextureModel.h"

class Process;

class CCuboOne : public CTextureModel
{
public:
	CCuboOne(void) : CTextureModel() { }
	~CCuboOne(void)		{ }

	void InsertModel(const std::string &primer, const std::string &second, int indc);
	std::vector<TextureVertex> loadModelToVertices(const std::string & modelname);// , std::vector<TextureVertex> &vertices);
	// float x, float y, float z);
	//void setColor(phyVector3D color) override { mColor = color; }
	//phyVector3D getColor() override { return mColor; }
	//phyVector3D getDeltaPosition() { return mDeltaPosition; }
	//phyVector3D getPosition() { return mPosition; }

	//void setDeltaPosition(const phyVector3D &dpos) { 
	//	mDeltaPosition.x = dpos.x;
	//	mDeltaPosition.y = dpos.y;
	//	mDeltaPosition.z = dpos.z;
	//}

	//void drawModel() override;
	void Release() override { CTextureModel::Release(); }

private:
	//phyVector3D mColor;
	//phyVector3D mDeltaPosition;
	//phyVector3D mPosition;
};
