#include <d3d11.h>
#include <cmath>
#include <new>
#include <iostream>
#include "CuboOne2.h"

#define TINYOBJLOADER_IMPLEMENTATION_2
#include "../loader/tiny_obj_loader2.h"

void CCuboOne2::InsertModel(const std::string &modelname, phyVector3D pos, phyVector3D sca, int indc)
{
	ind = indc;
	mSca.x = sca.x;
	mSca.y = sca.y;
	mSca.z = sca.z;

	if (mpDevice == nullptr)
		return;
	mDeltaPosition.x = 0.f;	mDeltaPosition.y = 0.f;	mDeltaPosition.z = 0.f;
	mPos.x = pos.x; mPos.y = pos.y; mPos.z = pos.z;

	tinyobj2::attrib_t attrib;
	std::vector<tinyobj2::shape_t> shapes;
	std::vector<tinyobj2::material_t> materials;

	std::string err;
	bool ret = tinyobj2::LoadObj(&attrib, &shapes, &materials, &err, modelname.c_str());

	if (!err.empty()) { // `err` may contain warning message.
		std::cerr << err << std::endl;
	}

	if (!ret) {
		exit(1);
	}

	std::vector<TextureVertex2> vertices;
	//int vertexc = 0;

	for (size_t s = 0; s < shapes.size(); s++) {
		// Loop over faces(polygon)
		size_t index_offset = 0;
		for (size_t f = 0; f < shapes[s].mesh.num_face_vertices.size(); f++) {
			int fv = shapes[s].mesh.num_face_vertices[f];

			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				// access to vertex
				
				tinyobj2::index_t idx = shapes[s].mesh.indices[index_offset + v];
				TextureVertex2 allv;
				allv.pos[0] = attrib.vertices[3 * idx.vertex_index + 0];			
				allv.pos[1] = attrib.vertices[3 * idx.vertex_index + 1];
				allv.pos[2] = attrib.vertices[3 * idx.vertex_index + 2];

				allv.normal[0] = attrib.normals[3 * idx.normal_index + 0];
				allv.normal[1] = attrib.normals[3 * idx.normal_index + 1];
				allv.normal[2] = attrib.normals[3 * idx.normal_index + 2];

				allv.texCoord[0] = attrib.texcoords[2 * idx.texcoord_index + 0];
				allv.texCoord[1] = attrib.texcoords[2 * idx.texcoord_index + 1];
				vertices.push_back(allv);
			}
			index_offset += fv;

			// per-face material
			//shapes[s].mesh.material_ids[f];
		}
	}




	//std::vector<TextureVertex> vertices {
	//	{ {-10.0f,  10.0f, -10.0f}, { 0.0f, 0.0f, -1.0f}, { 0.0f, 0.0f} },
	//	{ { 10.0f,  10.0f, -10.0f}, { 0.0f, 0.0f, -1.0f}, { 1.0f, 0.0f} },
	//	{ {-10.0f, -10.0f, -10.0f}, { 0.0f, 0.0f, -1.0f}, { 0.0f, 1.0f} },
	//	{ {-10.0f, -10.0f, -10.0f}, { 0.0f, 0.0f, -1.0f}, { 0.0f, 1.0f} },
	//	{ { 10.0f,  10.0f, -10.0f}, { 0.0f, 0.0f, -1.0f}, { 1.0f, 0.0f} },
	//	{ { 10.0f, -10.0f, -10.0f}, { 0.0f, 0.0f, -1.0f}, { 1.0f, 1.0f} },

	//	{ { 10.0f,  10.0f, -10.0f}, { 1.0f, 0.0f, 0.0f}, { 0.0f, 0.0f} },
	//	{ { 10.0f,  10.0f,  10.0f}, { 1.0f, 0.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ { 10.0f, -10.0f, -10.0f}, { 1.0f, 0.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ { 10.0f, -10.0f, -10.0f}, { 1.0f, 0.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ { 10.0f,  10.0f,  10.0f}, { 1.0f, 0.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ { 10.0f, -10.0f,  10.0f}, { 1.0f, 0.0f, 0.0f}, { 1.0f, 1.0f} },
	//	
	//	{ { 10.0f,  10.0f,  10.0f}, { 0.0f, 0.0f, 1.0f}, { 0.0f, 0.0f} },
	//	{ {-10.0f,  10.0f,  10.0f}, { 0.0f, 0.0f, 1.0f}, { 1.0f, 0.0f} },
	//	{ { 10.0f, -10.0f,  10.0f}, { 0.0f, 0.0f, 1.0f}, { 0.0f, 1.0f} },
	//	{ { 10.0f, -10.0f,  10.0f}, { 0.0f, 0.0f, 1.0f}, { 0.0f, 1.0f} },
	//	{ {-10.0f,  10.0f,  10.0f}, { 0.0f, 0.0f, 1.0f}, { 1.0f, 0.0f} },
	//	{ {-10.0f, -10.0f,  10.0f}, { 0.0f, 0.0f, 1.0f}, { 1.0f, 1.0f} },
	//	
	//	{ {-10.0f,  10.0f,  10.0f}, {-1.0f, 0.0f, 0.0f}, { 0.0f, 0.0f} },
	//	{ {-10.0f,  10.0f, -10.0f}, {-1.0f, 0.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ {-10.0f, -10.0f,  10.0f}, {-1.0f, 0.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ {-10.0f, -10.0f,  10.0f}, {-1.0f, 0.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ {-10.0f,  10.0f, -10.0f}, {-1.0f, 0.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ {-10.0f, -10.0f, -10.0f}, {-1.0f, 0.0f, 0.0f}, { 1.0f, 1.0f} },
	//	
	//	{ {-10.0f,  10.0f,  10.0f}, { 0.0f, 1.0f, 0.0f}, { 0.0f, 0.0f} },
	//	{ { 10.0f,  10.0f,  10.0f}, { 0.0f, 1.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ {-10.0f,  10.0f, -10.0f}, { 0.0f, 1.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ {-10.0f,  10.0f, -10.0f}, { 0.0f, 1.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ { 10.0f,  10.0f,  10.0f}, { 0.0f, 1.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ { 10.0f,  10.0f, -10.0f}, { 0.0f, 1.0f, 0.0f}, { 1.0f, 1.0f} },
	//															  
	//	{ {-10.0f, -10.0f, -10.0f}, { 0.0f, -1.0f, 0.0f}, { 0.0f, 0.0f} },
	//	{ { 10.0f, -10.0f, -10.0f}, { 0.0f, -1.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ {-10.0f, -10.0f,  10.0f}, { 0.0f, -1.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ {-10.0f, -10.0f,  10.0f}, { 0.0f, -1.0f, 0.0f}, { 0.0f, 1.0f} },
	//	{ { 10.0f, -10.0f, -10.0f}, { 0.0f, -1.0f, 0.0f}, { 1.0f, 0.0f} },
	//	{ { 10.0f, -10.0f,  10.0f}, { 0.0f, -1.0f, 0.0f}, { 1.0f, 1.0f} }
	//};

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());

	//std::vector<unsigned short> indices{
	//	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
	//	13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
	//	25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35
	//};
	std::vector<unsigned short> indices;
	for (size_t i = 0; i < vertices.size(); i++) {
		indices.push_back(i);
	}
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

//void CCuboOne::drawModel()
//{
////	//setRenderProperties();
////	activateBuffers();
////	//ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
////	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();
////
//	MathUtil::CMatrix translation =
//	MathUtil::MatrixTranslation(0.0f, mDeltaPosition.y, 0.0f);
//	//bufvdat.model = MathUtil::MatrixTranspose(translation);
//	//mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
//	//mpContext->VSSetConstantBuffers(0, 1, &conbuf);
//
//	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
////
//}