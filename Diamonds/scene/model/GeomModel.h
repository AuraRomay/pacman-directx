#ifndef GEOMMODEL_H
#define GEOMMODEL_H

//#include "UserModel.h"

struct GeomVertex
{
    float pos[3];
	float size;
};

struct ID3D11Buffer;
struct ID3D11Device;

class CGeomModel// : public UserModel
{
private:
	ID3D11Buffer *mpVertexBuffer;
	//ID3D11Buffer *mpIndexBuffer;
	//unsigned int mIndexBufferSize;

public:
	CGeomModel(void);
	~CGeomModel(void)		{ }

	void Initialize(ID3D11Device *device);
	ID3D11Buffer *GetVertices()			{ return mpVertexBuffer; }
	//ID3D11Buffer *GetIndices()			{ return mpIndexBuffer; }
	//unsigned int GetIndexBufferSize()	{ return mIndexBufferSize; }

	void Release();
};

#endif
