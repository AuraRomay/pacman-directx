#include <d3d11.h>
#include <cmath>
#include <assert.h>
#include "../Constants.h"
#include "BaseTextureModel.h"
#include "../../render/DXCamera.h"
#include "../../render/textures/TextureBase.h"

CBaseTextureModel::CBaseTextureModel(void)
: mpVertexBuffer(nullptr), mpIndexBuffer(nullptr)
, mIndexBufferSize(0), mpDevice(nullptr)
{
}

void CBaseTextureModel::Initialize(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mpDevice = device;
	mpContext = context;

	mpBaseShader.Initialize("SimpleShader",
		basicVertexLayoutDesc, ARRAYSIZE(basicVertexLayoutDesc), device);
}

void CBaseTextureModel::SetBuffers()
{
	if (mpDevice == nullptr)
		return;

	mIndexBufferSize = mAllModelsIndices.size();

	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(BasicTexVertex) * mAllModelsVertices.size();
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = &mAllModelsVertices[0];
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	mpDevice->CreateBuffer(
		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.ByteWidth = sizeof(unsigned short) * mAllModelsIndices.size();
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexBufferData;
	indexBufferData.pSysMem = &mAllModelsIndices[0];
	indexBufferData.SysMemPitch = 0;
	indexBufferData.SysMemSlicePitch = 0;

	mpDevice->CreateBuffer(
		&indexBufferDesc, &indexBufferData, &mpIndexBuffer);
}


void CBaseTextureModel::drawModel()
{
	//activateShaders();
	//activateTexture();
	//activateInputLayout();
	setRenderProperties();
	activateBuffers();
	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
}

//void CBaseTextureModel::activateShaders()
//{
//	ID3D11VertexShader *vshader = mpBaseShader.GetVertexShader();
//	ID3D11PixelShader *pshader = mpBaseShader.GetPixelShader();
//	mpContext->VSSetShader(vshader, nullptr, 0);
//	mpContext->PSSetShader(pshader, nullptr, 0);
//
//	ID3D11Buffer *conbuf = mpBaseShader.GetVConstantBuffer();
//	sVConstantBuffer &bufdat = mpBaseShader.GetVConstantBufferData();
//
//	DirectX::XMMATRIX nview = DXCamera::getInstance()->GetView();
//	DirectX::XMMATRIX nproj = DXCamera::getInstance()->GetProjection();
//	bufdat.view = DirectX::XMMatrixTranspose(nview);
//	bufdat.projection = DirectX::XMMatrixTranspose(nproj);
//
//	mpContext->UpdateSubresource(
//		conbuf, 0, nullptr, &bufdat, 0, 0);
//	mpContext->VSSetConstantBuffers(0, 1, &conbuf);
//}
//
//void CBaseTextureModel::activateTexture()
//{
//	TextureBase *texture = getTexture();
//	ID3D11ShaderResourceView *sres = texture->GetShaderResource();
//	ID3D11SamplerState *sste = texture->GetSampler();
//	mpContext->PSSetShaderResources(0, 1, &sres);
//	mpContext->PSSetSamplers(0, 1, &sste);
//
//	ID3D11InputLayout *inlay = mpBaseShader.GetInputLayout();
//	mpContext->IASetInputLayout(inlay);
//} 
//
//void CBaseTextureModel::activateInputLayout()
//{
//	ID3D11InputLayout *inlay = mpBaseShader.GetInputLayout();
//	mpContext->IASetInputLayout(inlay);
//}

void CBaseTextureModel::activateBuffers()
{
	unsigned int stride = sizeof(BasicTexVertex);
	unsigned int offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
	mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void CBaseTextureModel::setRenderProperties()
{
	ID3D11VertexShader *vshader = mpBaseShader.GetVertexShader();
	ID3D11PixelShader *pshader = mpBaseShader.GetPixelShader();
	mpContext->VSSetShader(vshader, nullptr, 0);
	mpContext->PSSetShader(pshader, nullptr, 0);

	ID3D11Buffer *conbuf = mpBaseShader.GetVConstantBuffer();
	sVConstantBuffer &bufdat = mpBaseShader.GetVConstantBufferData();
	if (conbuf == nullptr)
		return;

	MathUtil::CMatrix nview = DXCamera::getInstance()->GetView();
	MathUtil::CMatrix nproj = DXCamera::getInstance()->GetProjection();
	bufdat.view = MathUtil::MatrixTranspose(nview);
	bufdat.projection = MathUtil::MatrixTranspose(nproj);

	mpContext->UpdateSubresource(
		conbuf, 0, nullptr, &bufdat, 0, 0);
	mpContext->VSSetConstantBuffers(0, 1, &conbuf);

	conbuf = mpBaseShader.GetVAnimationBuffer();
	sAnimationCBuffer &animdat = mpBaseShader.GetVAnimationtBufferData();
	if (conbuf == nullptr)
		return;

	mpContext->UpdateSubresource(
		conbuf, 0, nullptr, &animdat, 0, 0);
	mpContext->VSSetConstantBuffers(1, 1, &conbuf);

	TextureBase *texture = getTexture();
	ID3D11ShaderResourceView *sres = texture->GetShaderResource();
	ID3D11SamplerState *sste = texture->GetSampler();
	mpContext->PSSetShaderResources(0, 1, &sres);
	mpContext->PSSetSamplers(0, 1, &sste);

	ID3D11InputLayout *inlay = mpBaseShader.GetInputLayout();
	mpContext->IASetInputLayout(inlay);
}

void CBaseTextureModel::Release()
{
	if (mpVertexBuffer != nullptr)
	{
		mpVertexBuffer->Release();
		mpVertexBuffer = nullptr;
	}
	if (mpIndexBuffer != nullptr)
	{
		mpIndexBuffer->Release();
		mpIndexBuffer = nullptr;
	}
}
