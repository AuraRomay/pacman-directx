#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "TextureModel2.h"
#include "../../render/shaders/ShaderTexture2.h"
#include "../../render/DXCamera.h"

CTextureModel2::CTextureModel2(void)
: mpVertexBuffer(nullptr), mpIndexBuffer(nullptr)
, mIndexBufferSize(0), mpDevice(nullptr)
{
}

void CTextureModel2::Initialize(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mpDevice = device;
	mpContext = context;
}

void CTextureModel2::SetBuffers()
{
	if (mpDevice == nullptr)
		return;

	mIndexBufferSize = mAllModelsIndices.size();

	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(TextureVertex2) * mAllModelsVertices.size();
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = &mAllModelsVertices[0];
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	HRESULT hr = mpDevice->CreateBuffer(
		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);

	if (mAllModelsIndices.size() > 0)
	{
		D3D11_BUFFER_DESC indexBufferDesc;
		indexBufferDesc.ByteWidth = sizeof(unsigned short) * mAllModelsIndices.size();
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexBufferData;
		indexBufferData.pSysMem = &mAllModelsIndices[0];
		indexBufferData.SysMemPitch = 0;
		indexBufferData.SysMemSlicePitch = 0;

		hr = mpDevice->CreateBuffer(
			&indexBufferDesc, &indexBufferData, &mpIndexBuffer);
	}
}

void CTextureModel2::drawModel()
{
	//setRenderProperties();
	activateBuffers();
	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
}

void CTextureModel2::activateBuffers()
{
	unsigned int stride = sizeof(TextureVertex2);
	unsigned int offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
	mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	
	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

//void CTextureModel::activateLineBuffers()
//{
//	unsigned int stride = sizeof(BasicPosVertex);
//	unsigned int offset = 0;
//	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
//	//mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
//	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
//}

void CTextureModel2::setRenderProperties(ShaderTexture2 *shader)
{
	shader->updateShaderVariables(mPos, mSca, ind);
}


void CTextureModel2::Release()
{
	if (mpVertexBuffer != nullptr)
	{
		mpVertexBuffer->Release();
		mpVertexBuffer = nullptr;
	}
	if (mpIndexBuffer != nullptr)
	{
		mpIndexBuffer->Release();
		mpIndexBuffer = nullptr;
	}
}
