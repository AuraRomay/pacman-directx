#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "../../render/textures/TextureMgr.h"
#include "../logic/ModelSets.h"
#include "Panel.h"

std::mutex CPanel::_mutex;
static CPanel *instance = nullptr;

CPanel *CPanel::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)		
			instance = new CPanel();		
	}
	return instance;
}

CPanel::CPanel(void)
: CBaseTextureModel()
{
}

unsigned short CPanel::InsertModel(float x, float y, float z, float wp, float hp,
	float u, float v, float wt, float ht)
{
	std::vector<BasicTexVertex> vertices{
		{ { x, y, z }, { u, v } },
		{ { x + wp, y, z }, { u + wt, v } },
		{ { x, y - hp, z }, { u, v + ht } },
		{ { x + wp, y - hp, z }, { u + wt, v + ht } }
	};

	std::vector<unsigned short> indices{
		0, 1, 3, 0, 3, 2,
	};
	
	for (unsigned short i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	unsigned short firstPanelIndex = mAllModelsIndices.size();
	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
	return firstPanelIndex;
}

void CPanel::drawModel()
{
	setRenderProperties();
	activateBuffers();
	//ID3D11Buffer *conbuf = mpBaseShader.GetVConstantBuffer();
	//sVConstantBuffer &bufvdat = mpBaseShader.GetVConstantBufferData();

	for (auto &texState : CModelSets::getInstance()->getTextureStates())
	{
		if (texState.second.isActive)
		{
			//DirectX::XMMATRIX translation =
			//	DirectX::XMMatrixTranslation(texState.second.posActual[0],
			//	texState.second.posActual[1], texState.second.posActual[2]);
			//mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
			//mpContext->VSSetConstantBuffers(0, 1, &conbuf);

			mpContext->DrawIndexed(6, texState.second.startIndex, 0);
		}
	}
}

TextureBase *CPanel::getTexture()
{
	return CTextureMgr::getInstance()->getTexture("panel");
}

void CPanel::Release()
{
	CBaseTextureModel::Release();
	delete instance;
}
