#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "Drain.h"

std::mutex CDrain::_mutex;
static CDrain *instance = nullptr;

CDrain *CDrain::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)		
			instance = new CDrain();		
	}
	return instance;
}

CDrain::CDrain(void)
: CBasePosModel()
{
}

void CDrain::InsertModel(float x0, float y0, float size)
{
	const float thickness = 0.2f;

	std::vector<BasicPosVertex> vertices {
		{ { x0, y0, -0.1f } },
		{ { x0 + size, y0, -0.1f } },
		{ { x0, y0 - thickness, -0.1f } },
		{ { x0 + size, y0 - thickness, -0.1f } }
	};

	std::vector<unsigned short> indices{
		0, 1, 3, 0, 3, 2,
	};

	for (unsigned short i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

void CDrain::Release()
{
	CBasePosModel::Release();
	delete instance;
}
