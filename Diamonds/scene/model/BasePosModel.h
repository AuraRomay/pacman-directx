#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "VertexDefs.h"
//#include "../../render/shaders/ShaderColor.h"
#include "../logic/UMath.h"
class ShaderColor;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Buffer;

class CBasePosModel
{
public:
	~CBasePosModel(void)		{ }

	void Initialize(ID3D11Device *device, ID3D11DeviceContext *context);

	ID3D11Buffer *GetVertices()			{ return mpVertexBuffer; }
	ID3D11Buffer *GetIndices()			{ return mpIndexBuffer; }
	unsigned int GetIndexBufferSize()	{ return mIndexBufferSize; }

	virtual void drawModel();
	virtual phyVector3D getColor()						{ return mColor; }
	virtual void setColor(phyVector3D color)	{ mColor = color; }

	virtual void Release();

	void setRenderProperties(ShaderColor *shader);
protected:
	CBasePosModel(void);
	void SetBuffers();
	void activateBuffers();
	void activateLineBuffers();

protected:
	//ShaderColor mpColorShader;

	std::vector<BasicPosVertex> mAllModelsVertices;
	std::vector<unsigned short> mAllModelsIndices;

	ID3D11Buffer *mpVertexBuffer;
	ID3D11Buffer *mpIndexBuffer;
	ID3D11Device *mpDevice;
	ID3D11DeviceContext *mpContext;
	unsigned int mIndexBufferSize;

private:
	phyVector3D mColor;
};
