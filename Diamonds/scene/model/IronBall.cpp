#include <d3d11.h>
#include <cmath>
#include <new>
#include "IronBall.h"
#include "../logic/ModelSets.h"
#include "../Constants.h"

std::mutex CIronBall::_mutex;
static CIronBall *instance = nullptr;

CIronBall *CIronBall::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)
			instance = new CIronBall();		
	}
	return instance;
}

CIronBall::CIronBall(void)
: CBasePosModel()
{
}

void CIronBall::InsertModel()
{
	if (mpDevice == nullptr)
		return;

	mAllModelsVertices.push_back({ { 0.0f, RADIOBALL, 0.0f } });
	for (int i = 1; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			float x = sinf(0.3927f*i) * cosf(0.7854f*j) * RADIOBALL;
			float y = cosf(0.3927f*i) * RADIOBALL;
			float z = sinf(0.3927f*i) * sinf(0.7854f*j) * RADIOBALL;
			mAllModelsVertices.push_back({ { x, y, z } });
		}
	}
	mAllModelsVertices.push_back({ { 0.0f, -RADIOBALL, 0.0f } });

	for (unsigned short j = 0; j < 7; j++)
	{
		mAllModelsIndices.push_back(0);
		mAllModelsIndices.push_back(j + 2);
		mAllModelsIndices.push_back(j + 1);
	}
	mAllModelsIndices.push_back(0);
	mAllModelsIndices.push_back(1);
	mAllModelsIndices.push_back(8);

	for (unsigned short i = 1; i < 42; i += 8)
	{
		for (unsigned short j = 0; j < 7; j++)
		{
			mAllModelsIndices.push_back(j + i);
			mAllModelsIndices.push_back(j + i + 1);
			mAllModelsIndices.push_back(j + i + 8);

			mAllModelsIndices.push_back(j + i + 1);
			mAllModelsIndices.push_back(j + i + 9);
			mAllModelsIndices.push_back(j + i + 8);
		}
		mAllModelsIndices.push_back(i + 7);
		mAllModelsIndices.push_back(i);
		mAllModelsIndices.push_back(i + 15);

		mAllModelsIndices.push_back(i);
		mAllModelsIndices.push_back(i + 8);
		mAllModelsIndices.push_back(i + 15);
	}

	for (unsigned short j = 49; j < 56; j++)
	{
		mAllModelsIndices.push_back(57);
		mAllModelsIndices.push_back(j);
		mAllModelsIndices.push_back(j + 1);
	}
	mAllModelsIndices.push_back(56);
	mAllModelsIndices.push_back(49);
	mAllModelsIndices.push_back(57);

	SetBuffers();
}

void CIronBall::drawModel()
{
	setRenderProperties();
	activateBuffers();
	ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
	sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();

	for (auto ball : CModelSets::getInstance()->getBallVectors())
	{
		//DirectX::XMMATRIX translation =
		//	DirectX::XMMatrixTranslation(ball.second.posActual[0], ball.second.posActual[1], 0.0f);
		//bufvdat.model = DirectX::XMMatrixTranspose(translation);
		MathUtil::CMatrix translation =
			MathUtil::MatrixTranslation(ball.second.posActual[0], ball.second.posActual[1], 0.0f);
		bufvdat.model = MathUtil::MatrixTranspose(translation);
		mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
		mpContext->VSSetConstantBuffers(0, 1, &conbuf);

		mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
	}
}

void CIronBall::Release()
{
	CBasePosModel::Release();
	delete instance;
}

