#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "VertexDefs.h"
#include "../../render/shaders/ShaderBase.h"
#include "../../render/textures/TextureBase.h"

class CBaseTextureModel
{
public:
	~CBaseTextureModel(void)		{ }

	void Initialize(ID3D11Device *device, ID3D11DeviceContext *context);

	ID3D11Buffer *GetVertices()			{ return mpVertexBuffer; }
	ID3D11Buffer *GetIndices()			{ return mpIndexBuffer; }
	unsigned int GetIndexBufferSize()	{ return mIndexBufferSize; }

	virtual void drawModel();
	virtual TextureBase *getTexture() { return nullptr; }

	virtual void Release();

protected:
	CBaseTextureModel(void);
	void SetBuffers();	
	void activateBuffers();
	//void activateShaders();
	//void activateTexture();
	//void activateInputLayout();
	void setRenderProperties();

protected:
	ShaderBase mpBaseShader;

	std::vector<BasicTexVertex> mAllModelsVertices;
	std::vector<unsigned short> mAllModelsIndices;

	ID3D11Buffer *mpVertexBuffer;
	ID3D11Buffer *mpIndexBuffer;
	ID3D11Device *mpDevice;
	ID3D11DeviceContext *mpContext;
	unsigned int mIndexBufferSize;
};
