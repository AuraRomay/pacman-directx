#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "TextureModel.h"
#include "../../render/shaders/ShaderTexture.h"
#include "../../render/DXCamera.h"

CTextureModel::CTextureModel(void)
: mpVertexBuffer(nullptr), mpIndexBuffer(nullptr)
, mIndexBufferSize(0), mpDevice(nullptr)
, mRotateAngles{0.f, 0.f, 0.f}
{
}

void CTextureModel::Initialize(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mpDevice = device;
	mpContext = context;
}

void CTextureModel::SetBuffers()
{
	if (mpDevice == nullptr)
		return;

	mIndexBufferSize = mAllModelsIndices.size();

	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(KeyframeVertex) * mAllModelsVertices.size();
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = &mAllModelsVertices[0];
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	HRESULT hr = mpDevice->CreateBuffer(
		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);

	if (mAllModelsIndices.size() > 0)
	{
		D3D11_BUFFER_DESC indexBufferDesc;
		indexBufferDesc.ByteWidth = sizeof(unsigned short) * mAllModelsIndices.size();
		indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		indexBufferDesc.CPUAccessFlags = 0;
		indexBufferDesc.MiscFlags = 0;
		indexBufferDesc.StructureByteStride = 0;

		D3D11_SUBRESOURCE_DATA indexBufferData;
		indexBufferData.pSysMem = &mAllModelsIndices[0];
		indexBufferData.SysMemPitch = 0;
		indexBufferData.SysMemSlicePitch = 0;

		hr = mpDevice->CreateBuffer(
			&indexBufferDesc, &indexBufferData, &mpIndexBuffer);
	}
}

void CTextureModel::drawModel()
{
	//setRenderProperties();
	activateBuffers();
	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
}

void CTextureModel::activateBuffers()
{
	unsigned int stride = sizeof(KeyframeVertex);
	unsigned int offset = 0;
	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
	mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	
	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

//void CTextureModel::activateLineBuffers()
//{
//	unsigned int stride = sizeof(BasicPosVertex);
//	unsigned int offset = 0;
//	mpContext->IASetVertexBuffers(0, 1, &mpVertexBuffer, &stride, &offset);
//	//mpContext->IASetIndexBuffer(mpIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
//	mpContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
//}

void CTextureModel::setRenderProperties(ShaderTexture *shader)
{
	shader->updateShaderVariables(mPosition, mRotateAngles, ind);
}


void CTextureModel::Release()
{
	if (mpVertexBuffer != nullptr)
	{
		mpVertexBuffer->Release();
		mpVertexBuffer = nullptr;
	}
	if (mpIndexBuffer != nullptr)
	{
		mpIndexBuffer->Release();
		mpIndexBuffer = nullptr;
	}
}
