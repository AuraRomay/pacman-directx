#ifndef VERTEXDEFS2_H
#define VERTEXDEFS2_H

struct BasicTexVertex2
{
	float pos[3];
	float tex[2];
};

struct BasicPosVertex2
{
	float pos[3];
};

struct TextureVertex2
{
	float pos[3];
	float normal[3];
	float texCoord[2];
};

struct KeyframeVertex2
{
	float pos[4];
	float normal[3];
	float texCoord[2];
	float nxtPos[3];
	float nxtNormal[3];
};

struct NormalMapVertex2
{
	float pos[3];
	float texCoord[2];
	float normal[3];
	float tangent[4];
};

#endif
