#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "../../render/textures/TextureMgr.h"
#include "../logic/ModelSets.h"
#include "Score.h"

std::mutex CScore::_mutex;
static CScore *instance = nullptr;

CScore *CScore::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)		
			instance = new CScore();		
	}
	return instance;
}

CScore::CScore(void)
: CBaseTextureModel(), mXAbsOffset(0.f)
{
}

void CScore::InsertModel(float x0, float y0, float x1, float y1)
{
	mXAbsOffset = x1 - x0;
	std::vector<BasicTexVertex> vertices;
	for (int j = 0; j < 3; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			vertices.push_back({ { x0, y0, 1.1f }, { i*0.22656f, j*0.22656f } });
			vertices.push_back({ { x1, y0, 1.1f }, { (i + 1)*0.22656f, j*0.22656f } });
			vertices.push_back({ { x0, y1, 1.1f }, { i*0.22656f, (j + 1)*0.22656f } });
			vertices.push_back({ { x1, y1, 1.1f }, { (i + 1)*0.22656f, (j + 1)*0.22656f } });
		}
	}
	std::vector<unsigned short> indices;
	for (unsigned short k = 0; k < 48; k+=4)
	{
		indices.push_back(0 + k);
		indices.push_back(1 + k);
		indices.push_back(3 + k);
		indices.push_back(0 + k);
		indices.push_back(3 + k);
		indices.push_back(2 + k);
	}

	mAllModelsVertices.clear();
	mAllModelsIndices.clear();
	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

float CScore::getXOffset(int relativePosition)
{
	return -(mXAbsOffset * relativePosition);
}

unsigned int CScore::getIndexForNumber(int number)
{
	return 6 * number;
}

void CScore::drawModel()
{
	setRenderProperties();
	activateBuffers();
	ID3D11Buffer *conbuf = mpBaseShader.GetVConstantBuffer();
	sVConstantBuffer &bufvdat = mpBaseShader.GetVConstantBufferData();

	int pos = 0;
	for (auto num : CModelSets::getInstance()->getScoreVectors())
	{
		float xOffset = CScore::getInstance()->getXOffset(pos++);
		unsigned int indexOffset = CScore::getInstance()->getIndexForNumber(num);

		//DirectX::XMMATRIX translation =
		//	DirectX::XMMatrixTranslation(xOffset, 0.0f, 0.0f);
		//bufvdat.model = DirectX::XMMatrixTranspose(translation);
		MathUtil::CMatrix translation =
			MathUtil::MatrixTranslation(xOffset, 0.0f, 0.0f);
		bufvdat.model = MathUtil::MatrixTranspose(translation);
		mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
		mpContext->VSSetConstantBuffers(0, 1, &conbuf);

		// DIBUJADO
		//mpDcontext->DrawIndexed(indexbufsz, 0, 0);
		mpContext->DrawIndexed(6, indexOffset, 0);
	}
}

TextureBase *CScore::getTexture()
{
	return CTextureMgr::getInstance()->getTexture("nums");
}

void CScore::Release()
{
	CBaseTextureModel::Release();
	delete instance;
}