#include <d3d11.h>
#include <cmath>
#include <new>
#include "FrontFlake.h"
//#include "../logic/ModelSets.h"
//#include "../Constants.h"


CFrontFlake::CFrontFlake(void)
	: CBaseFlake()
{
}

void CFrontFlake::InsertModel(float x, float y)
{
	if (mpDevice == nullptr)
		return;

	std::vector<BasicPosVertex> vertices{
			{ { x - 10.f, y + 10.f, 0.f }	},
			{ { x + 10.f, y + 10.f, 0.f }	},
			{ { x - 10.f, y - 10.f, 0.f }	},
			{ { x + 10.f, y - 10.f, 0.f }	}
	};
	std::vector<unsigned short> indices{
		0, 1, 3, 0, 3, 2,
	};

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

//void CFrontFlake::drawModel()
//{
//	setRenderProperties();
//	activateBuffers();
//	//ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
//	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();
//
//	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
//}

