#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "../../render/textures/TextureMgr.h"
#include "../logic/ModelSets.h"
#include "Card.h"

CCard::CCard(void)
: CBaseTextureModel()
{
}

std::mutex CCard::_mutex;
static CCard *instance = nullptr;

CCard *CCard::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)
			instance = new CCard();
	}
	return instance;
}

void CCard::InsertModel(/*float x, float y, float z, */float wp, float hp,
	float u, float v, float wt, float ht)
{
	mUOffset = wt;
	mVOffset = ht;

	std::vector<BasicTexVertex> vertices{
		{ { 0.f, 0.f, 0.f }, { u, v } },
		{ { wp, 0.f, 0.f }, { u + wt, v } },
		{ { 0.f, -hp, 0.f }, { u, v + ht } },
		{ { wp, -hp, 0.f }, { u + wt, v + ht } }
	};

	//std::vector<BasicTexVertex> vertices{
	//	{ { x, y, z }, { u, v } },
	//	{ { x + wp, y, z }, { u + wt, v } },
	//	{ { x, y - hp, z }, { u, v + ht } },
	//	{ { x + wp, y - hp, z }, { u + wt, v + ht } }
	//};

	std::vector<unsigned short> indices{
		0, 1, 3, 0, 3, 2,
	};
	
	//for (unsigned int i = 0; i < indices.size(); i++)
	//	indices[i] += mAllModelsVertices.size();

	//unsigned short firstPanelIndex = mAllModelsIndices.size();
	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
	//return firstPanelIndex;
}

void CCard::drawModel(int index)
{
	setRenderProperties();
	activateBuffers();
	ID3D11Buffer *conbuf = mpBaseShader.GetVConstantBuffer();
	sVConstantBuffer &bufvdat = mpBaseShader.GetVConstantBufferData();
	ID3D11Buffer *anibuf = mpBaseShader.GetVAnimationBuffer();
	sAnimationCBuffer &anidat = mpBaseShader.GetVAnimationtBufferData();

	auto &cardsMap = CModelSets::getInstance()->getCardStates();
	//if (cardsMap.count(index) == 0)
	//	return;
	auto &card = cardsMap.at(0);
	if (card.isActive)
	{
		MathUtil::CMatrix translation = 
			//MathUtil::MatrixTranslation(card.posActual[0], card.posActual[1], card.posActual[2]);
			MathUtil::MatrixRotationX(MathUtil::Pi/2.0f);
			bufvdat.model = MathUtil::MatrixTranspose(translation);
		mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
		mpContext->VSSetConstantBuffers(0, 1, &conbuf);

		anidat.uOffset[0] = card.actualFrame * mUOffset;
		mpContext->UpdateSubresource(anibuf, 0, nullptr, &anidat, 0, 0);
		mpContext->VSSetConstantBuffers(1, 1, &anibuf);

		mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
	}
}

TextureBase *CCard::getTexture()
{
	return CTextureMgr::getInstance()->getTexture("panel");
}

void CCard::Release()
{
	CBaseTextureModel::Release();
}
