#include <d3d11.h>
#include <cmath>
#include "../Constants.h"
#include "../../render/textures/TextureMgr.h"
#include "../logic/ModelSets.h"
#include "CardsSet.h"

std::mutex CCardsSet::_mutex;
static CCardsSet *instance = nullptr;

CCardsSet *CCardsSet::getInstance()
{
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)		
			instance = new CCardsSet();		
	}
	return instance;
}

CCardsSet::CCardsSet(void)
{
}

void CCardsSet::Initialize(ID3D11Device *device, ID3D11DeviceContext *context)
{
	mpDevice = device;
	mpContext = context;
}

void CCardsSet::InsertModel(int index, /*float x, float y, float z, */float wp, float hp,
	float u, float v, float wt, float ht)
{
	CCard *card = new CCard();
	card->Initialize(mpDevice, mpContext);
	card->InsertModel(wp, hp, u, v, wt, ht);
	mmCards.insert(std::make_pair(index, card));
}

void CCardsSet::drawModels()
{
	if (mmCards.size() > 0)
		for (auto card : mmCards)
			card.second->drawModel(card.first);
}

void CCardsSet::Release()
{
	for (auto card : mmCards)
	{
		card.second->Release();
		delete card.second;
		card.second = nullptr;
	}
	delete instance;
}
