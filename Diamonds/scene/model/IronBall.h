#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BasePosModel.h"

class CIronBall : public CBasePosModel
{
public:
	~CIronBall(void)		{ }

	static CIronBall *getInstance();
	void InsertModel();
	void drawModel() override;
	phyVector3D getColor() override { return{ 0.72f, 0.72f, 0.72f }; }

	void Release() override;

private:
	CIronBall(void);

private:
	static std::mutex _mutex;

};
