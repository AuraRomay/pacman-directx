#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BaseTextureModel.h"

class CCard : public CBaseTextureModel
{
public:
	~CCard(void)		{ }
	CCard(void);

	static CCard *getInstance();

	//static CCards *getInstance();
	void InsertModel(/*float x, float y, float z, */float wp, float hp,
		float u, float v, float wt, float ht);
	
	void drawModel(int index);
	TextureBase *getTexture() override;
	void Release() override;

private:
	float mUOffset;
	float mVOffset;

	static std::mutex _mutex;

};
