#include <d3d11.h>
#include <cmath>
#include <new>
#include "IronGrid.h"

//std::mutex CIronGrid::_mutex;
//static CIronGrid *instance = nullptr;

//CIronGrid *CIronGrid::getInstance()
//{
//	if (!instance)
//	{
//		std::lock_guard<std::mutex> lock(_mutex);
//		if (!instance)
//			instance = new CIronGrid();		
//	}
//	return instance;
//}

//CIronGrid::CIronGrid(void)
//: CBasePosModel()
//{
//}

void CIronGrid::InsertModel()
{
	if (mpDevice == nullptr)
		return;

	for (float i = 0.f; i < 200.f; i+=5) {		
			mAllModelsVertices.push_back({ { -100.f, -60.f, 100.f - i} });
			mAllModelsVertices.push_back({ { 100.f, -60.f, 100.f - i} });
			mAllModelsVertices.push_back({ { -100.f + i, -60.f, 100.f} });
			mAllModelsVertices.push_back({ { -100.f + i, -60.f, -100.f} });
	}
	SetBuffers();
}

void CIronGrid::drawModel()
{
	//setRenderProperties();
	activateLineBuffers();
	//ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();
	
	mpContext->Draw(mAllModelsVertices.size(), 0);	
}

//void CIronGrid::Release()
//{
//	CBasePosModel::Release();
//	//delete instance;
//}

