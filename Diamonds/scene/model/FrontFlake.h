#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BaseFlake.h"

class CFrontFlake : public CBaseFlake
{
public:
	CFrontFlake(void);
	~CFrontFlake(void)		{ }

	void InsertModel(float x, float y) override;
	//void drawModel() override;

};
