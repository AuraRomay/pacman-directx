#pragma once
//#include <vector>
//#include <memory>
//#include <mutex>
#include <string>
#include "TextureModel2.h"

class Process;

class CCuboOne2 : public CTextureModel2
{
public:
	CCuboOne2(void) : CTextureModel2() { }
	~CCuboOne2(void)		{ }

	void InsertModel(const std::string &modelname, phyVector3D pos, phyVector3D sca, int ind);// float x, float y, float z);
	//void setColor(phyVector3D color) override { mColor = color; }
	//phyVector3D getColor() override { return mColor; }
	phyVector3D getDeltaPosition() { return mDeltaPosition; }
	phyVector3D getPosition() { return mPosition; }
	int index() { return ind; }

	void setDeltaPosition(const phyVector3D &dpos) { 
		mDeltaPosition.x = dpos.x;
		mDeltaPosition.y = dpos.y;
		mDeltaPosition.z = dpos.z;
	}

	
	//void drawModel() override;
	void Release() override { CTextureModel2::Release(); }

private:
	//phyVector3D mColor;
	phyVector3D mDeltaPosition;
	phyVector3D mPosition;
	
};
