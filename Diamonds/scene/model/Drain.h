#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BasePosModel.h"

class CDrain : public CBasePosModel
{
public:
	~CDrain(void)		{ }
	static CDrain *getInstance();
	void InsertModel(float x0, float y0, float size);
	phyVector3D getColor() override { return{ 0.1f, 0.1f, 0.1f }; }

	void Release() override;
private:
	CDrain(void);

private:
	static std::mutex _mutex;

};
