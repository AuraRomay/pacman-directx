#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BasePosModel.h"

class Process;

class CDiamond : public CBasePosModel
{
public:
	CDiamond(void) : CBasePosModel() { }
	~CDiamond(void)		{ }

	void InsertModel(float x, float y, float z);
	//void setColor(phyVector3D color) override { mColor = color; }
	//phyVector3D getColor() override { return mColor; }
	phyVector3D getDeltaPosition() { return mDeltaPosition; }
	phyVector3D getPosition() { return mPosition; }

	void setDeltaPosition(const phyVector3D &dpos) { 
		mDeltaPosition.x = dpos.x;
		mDeltaPosition.y = dpos.y;
		mDeltaPosition.z = dpos.z;
	}

	//void drawModel() override;
	void Release() override { CBasePosModel::Release(); }

private:
	//phyVector3D mColor;
	phyVector3D mDeltaPosition;
	phyVector3D mPosition;
};
