#ifndef TALLERMODEL_H
#define TALLERMODEL_H
#include <vector>
#include <memory>
#include <mutex>

struct BasicVertex
{
	float pos[3];
	//float color[4];
	float tex[2];
};

struct ID3D11Buffer;
struct ID3D11Device;

class CPin
{
public:
	~CPin(void)		{ }

	static std::shared_ptr<CPin>& getInstance();
	void Initialize(ID3D11Device *device);

	void InsertModel(float xOffset, float yOffset);

	ID3D11Buffer *GetVertices()			{ return mpVertexBuffer; }
	ID3D11Buffer *GetIndices()			{ return mpIndexBuffer; }
	unsigned int GetIndexBufferSize()	{ return mIndexBufferSize; }

	void Release();

private:
	CPin(void);
	void SetBuffers(ID3D11Device *device);

private:
	ID3D11Buffer *mpVertexBuffer;
	ID3D11Buffer *mpIndexBuffer;
	ID3D11Device *mpDevice;
	unsigned int mIndexBufferSize;

	std::vector<BasicVertex> mAllModelsVertices;
	std::vector<unsigned short> mAllModelsIndices;

	static std::mutex _mutex;

};

#endif
