#include <d3d11.h>
#include <cmath>
#include <new>
#include "Diamond.h"

void CDiamond::InsertModel(float x, float y, float z)
{
	if (mpDevice == nullptr)
		return;
	mDeltaPosition.x = 0.f;	mDeltaPosition.y = 0.f;	mDeltaPosition.z = 0.f;
	mPosition.x = x; mPosition.y = y; mPosition.z = z;

	std::vector<BasicPosVertex> vertices{
		{ { x, y + 10.f, z } },

		{ { x, y, z - 10.f } },
		{ { x + 10.f, y, z } },
		{ { x, y, z + 10.f } },
		{ { x - 10.f, y, z } },

		{ { x, y - 10.f, z } },
	};
	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());

	std::vector<unsigned short> indices{
		0, 2, 1,  0, 3, 2,  0, 4, 3,  0, 1, 4,
		5, 1, 2,  5, 2, 3,  5, 3, 4,  5, 4, 1
	};
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	SetBuffers();
}

//void CDiamond::drawModel()
//{
//	//setRenderProperties();
//	activateBuffers();
//	//ID3D11Buffer *conbuf = mpColorShader.GetVConstantBuffer();
//	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();
//
//	//MathUtil::CMatrix translation =
//	//	MathUtil::MatrixTranslation(0.0f, mDeltaPosition.y, 0.0f);
//	//bufvdat.model = MathUtil::MatrixTranspose(translation);
//	//mpContext->UpdateSubresource(conbuf, 0, nullptr, &bufvdat, 0, 0);
//	//mpContext->VSSetConstantBuffers(0, 1, &conbuf);
//
//	mpContext->DrawIndexed(mIndexBufferSize, 0, 0);
//
//}