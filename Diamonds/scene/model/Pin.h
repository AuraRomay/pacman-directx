#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BasePosModel.h"

class CPin : public CBasePosModel
{
public:
	~CPin(void)		{ }

	static CPin *getInstance();
	void InsertModel(float xOffset, float yOffset);
	phyVector3D getColor() override { return{ 1.0f, 0.847f, 0.4f }; }

	void Release() override;

private:
	CPin(void);

private:
	static std::mutex _mutex;
};
