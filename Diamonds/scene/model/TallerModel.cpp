#include <d3d11.h>
#include "../Constants.h"
#include "Pin.h"

std::mutex CPin::_mutex;

std::shared_ptr<CPin>& CPin::getInstance()
{
	static std::shared_ptr<CPin> instance = nullptr;
	if (!instance)
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!instance)
			instance.reset(new CPin());
	}
	return instance;
}

CPin::CPin(void)
: mpVertexBuffer(nullptr), mpIndexBuffer(nullptr)
, mIndexBufferSize(0), mpDevice(nullptr)
{
}

void CPin::Initialize(ID3D11Device *device)
{
	mpDevice = device;
}

void CPin::InsertModel(float xOffset, float yOffset)
{
	if (mpDevice == nullptr)
		return;

	const float xyScale = RADIOPIN, zScale = 2.0f;
	const float angleDivided = 0.5236f / 8.f;

	std::vector<BasicVertex> vertices;
	std::vector<unsigned short> indices;

	vertices.push_back({ { xOffset, yOffset, 0.0f }, { 0.0f, 0.0f } });
	for (int i = 1; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			float x = (sinf(angleDivided*i) * cosf(0.7854f*j)) + xOffset;
			float y = (sinf(angleDivided*i) * sinf(0.7854f*j)) + yOffset;
			float z = 1.f-cosf(angleDivided*i);
			vertices.push_back({ { x, y, z }, { 0.125f*j, 0.125f*i } });
		}
	}

	for (int j = 0; j < 7; j++)
	{
		indices.push_back(0);
		indices.push_back(j + 2);
		indices.push_back(j + 1);
	}
	indices.push_back(0);
	indices.push_back(1);
	indices.push_back(8);

	for (int i = 1; i < 42; i += 8)
	{
		for (int j = 0; j < 7; j++)
		{
			indices.push_back(j + i);
			indices.push_back(j + i + 1);
			indices.push_back(j + i + 8);

			indices.push_back(j + i + 1);
			indices.push_back(j + i + 9);
			indices.push_back(j + i + 8);
		}
		indices.push_back(i + 7);
		indices.push_back(i);
		indices.push_back(i + 15);

		indices.push_back(i);
		indices.push_back(i + 8);
		indices.push_back(i + 15);
	}

	for (unsigned int i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	vertices.clear();
	indices.clear();

	const float cylinderZPushBack = 0.05f;
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			float x = (cosf(0.7854f*j) * xyScale) + xOffset;
			float y = (sinf(0.7854f*j) * xyScale) + yOffset;
			float z = ((float) i + cylinderZPushBack) * zScale;
			vertices.push_back({ { x, y, z }, { 0.125f*j, (float) i } });
		}
	}

	for (int j = 0; j < 7; j++)
	{
		indices.push_back(j);
		indices.push_back(j + 1);
		indices.push_back(j + 8);

		indices.push_back(j + 1);
		indices.push_back(j + 9);
		indices.push_back(j + 8);
	}
	indices.push_back(7);
	indices.push_back(0);
	indices.push_back(15);

	indices.push_back(0);
	indices.push_back(8);
	indices.push_back(15);

	for (unsigned int i = 0; i < indices.size(); i++)
		indices[i] += mAllModelsVertices.size();

	mAllModelsVertices.insert(mAllModelsVertices.end(), vertices.begin(), vertices.end());
	mAllModelsIndices.insert(mAllModelsIndices.end(), indices.begin(), indices.end());

	Release();
	SetBuffers(mpDevice);
}

void CPin::SetBuffers(ID3D11Device *device)
{


	//BasicVertex cuadroVertices[] = {
	//	{ { -0.5f, 0.0f, -0.5f }, { 0.0f, 0.5f } },
	//	{ { 0.5f, 0.0f, -0.5f }, { 0.25f, 0.5f } },
	//	{ { 0.5f, 0.0f, 0.5f }, { 0.5f, 0.5f } },
	//	{ { -0.5f, 0.0f, 0.5f }, { 0.75f, 0.5f } },
	//	{ { 0.0f, 1.0f, 0.0f }, { 0.5f, 0.0f } },
	//	{ { 0.0f, -1.0f, 0.0f }, { 0.5f, 1.0f } }
	//};

	//unsigned short cuadroIndices[] = {
	//	0, 4, 1,
	//	1, 4, 2,
	//	2, 4, 3,
	//	3, 4, 0,
	//	0, 1, 5,
	//	1, 2, 5,
	//	2, 3, 5,
	//	3, 0, 5
	//};

	mIndexBufferSize = mAllModelsIndices.size();

	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(BasicVertex) * mAllModelsVertices.size();// ARRAYSIZE(cuadroVertices);
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA vertexBufferData;
	vertexBufferData.pSysMem = &mAllModelsVertices[0];// cuadroVertices;
	vertexBufferData.SysMemPitch = 0;
	vertexBufferData.SysMemSlicePitch = 0;

	device->CreateBuffer(
		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);


	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.ByteWidth = sizeof(unsigned short) * mAllModelsIndices.size();// ARRAYSIZE(cuadroIndices);
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA indexBufferData;
	indexBufferData.pSysMem = &mAllModelsIndices[0];// cuadroIndices;
	indexBufferData.SysMemPitch = 0;
	indexBufferData.SysMemSlicePitch = 0;

	device->CreateBuffer(
		&indexBufferDesc, &indexBufferData, &mpIndexBuffer);
}

//void CPin::Initialize(ID3D11Device *device)
//{
//	//BasicVertex cuadroVertices[] = {
//	//	{ {-0.5f, -0.5f, 0.f}, {0.0f, 1.0f} },
//	//	{ { 0.5f, -0.5f, 0.f}, {1.0f, 1.0f} },
//	//	{ { 0.5f,  0.5f, 0.f}, {1.0f, 0.0f} },
//	//	{ {-0.5f,  0.5f, 0.f}, {0.0f, 0.0f} }
//	//};
//
//	//unsigned short cuadroIndices[] = {
//	//	0, 2, 1,
//	//	0, 3, 2
//	//};
//
//	//BasicVertex cuadroVertices[] = {
//	//	{ {-0.5f, -0.5f, 0.f}, {0.0f, 0.0f, 1.0f, 1.0f} },
//	//	{ { 0.5f, -0.5f, 0.f}, {0.0f, 1.0f, 0.0f, 1.0f} },
//	//	{ { 0.5f,  0.5f, 0.f}, {0.0f, 1.0f, 1.0f, 1.0f} },
//	//	{ {-0.5f,  0.5f, 0.f}, {1.0f, 0.0f, 0.0f, 1.0f} }
//	//};
//
//	//unsigned short cuadroIndices[] = {
//	//	0, 2, 1,
//	//	0, 3, 2
//	//};
//
//	//BasicVertex cuadroVertices[] = {
//	//	{ {-0.5f,  0.0f, -0.5f}, {0.0f, 0.0f, 1.0f, 1.0f} },
//	//	{ { 0.5f,  0.0f, -0.5f}, {0.0f, 1.0f, 0.0f, 1.0f} },
//	//	{ { 0.5f,  0.0f,  0.5f}, {0.0f, 1.0f, 1.0f, 1.0f} },
//	//	{ {-0.5f,  0.0f,  0.5f}, {1.0f, 0.0f, 0.0f, 1.0f} },
//	//	{ { 0.0f,  1.0f,  0.0f}, {1.0f, 1.0f, 0.0f, 1.0f} },
//	//	{ { 0.0f, -1.0f,  0.0f}, {1.0f, 0.0f, 1.0f, 1.0f} }
//	//};
//
//	//unsigned short cuadroIndices[] = {
//	//	0, 4, 1,
//	//	1, 4, 2,
//	//	2, 4, 3,
//	//	3, 4, 0,
//	//	0, 1, 5,
//	//	1, 2, 5,
//	//	2, 3, 5,
//	//	3, 0, 5
//	//};
//
//	BasicVertex cuadroVertices [] = {
//		{ { -0.5f, 0.0f, -0.5f }, { 0.0f, 0.5f } },
//		{ { 0.5f, 0.0f, -0.5f }, { 0.25f, 0.5f } },
//		{ { 0.5f, 0.0f, 0.5f }, { 0.5f, 0.5f } },
//		{ { -0.5f, 0.0f, 0.5f }, { 0.75f, 0.5f } },
//		{ { 0.0f, 1.0f, 0.0f }, { 0.5f, 0.0f } },
//		{ { 0.0f, -1.0f, 0.0f }, { 0.5f, 1.0f } }
//	};
//
//	unsigned short cuadroIndices [] = {
//		0, 4, 1,
//		1, 4, 2,
//		2, 4, 3,
//		3, 4, 0,
//		0, 1, 5,
//		1, 2, 5,
//		2, 3, 5,
//		3, 0, 5
//	};
//
//	mIndexBufferSize = ARRAYSIZE(cuadroIndices);
//
//	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
//	vertexBufferDesc.ByteWidth = sizeof(BasicVertex) * ARRAYSIZE(cuadroVertices);
//	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vertexBufferDesc.CPUAccessFlags = 0;
//	vertexBufferDesc.MiscFlags = 0;
//	vertexBufferDesc.StructureByteStride = 0;
//
//	D3D11_SUBRESOURCE_DATA vertexBufferData;
//	vertexBufferData.pSysMem = cuadroVertices;
//	vertexBufferData.SysMemPitch = 0;
//	vertexBufferData.SysMemSlicePitch = 0;
//
//	device->CreateBuffer(
//		&vertexBufferDesc, &vertexBufferData, &mpVertexBuffer);
//
//
//	D3D11_BUFFER_DESC indexBufferDesc;
//	indexBufferDesc.ByteWidth = sizeof(unsigned short) * ARRAYSIZE(cuadroIndices);
//	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	indexBufferDesc.CPUAccessFlags = 0;
//	indexBufferDesc.MiscFlags = 0;
//	indexBufferDesc.StructureByteStride = 0;
//
//	D3D11_SUBRESOURCE_DATA indexBufferData;
//	indexBufferData.pSysMem = cuadroIndices;
//	indexBufferData.SysMemPitch = 0;
//	indexBufferData.SysMemSlicePitch = 0;
//
//	device->CreateBuffer(
//		&indexBufferDesc, &indexBufferData, &mpIndexBuffer);
//}

void CPin::Release()
{
	if (mpVertexBuffer != nullptr)
	{
		mpVertexBuffer->Release();
		mpVertexBuffer = nullptr;
	}
	if (mpIndexBuffer != nullptr)
	{
		mpIndexBuffer->Release();
		mpIndexBuffer = nullptr;
	}
}

//BasicVertex cuadroVertices[] = {
//	{ {-0.5f,  0.0f, -0.5f}, {0.0f, 0.0f, 1.0f, 1.0f} },
//	{ { 0.5f,  0.0f, -0.5f}, {0.0f, 1.0f, 0.0f, 1.0f} },
//	{ { 0.5f,  0.0f,  0.5f}, {0.0f, 1.0f, 1.0f, 1.0f} },
//	{ {-0.5f,  0.0f,  0.5f}, {1.0f, 0.0f, 0.0f, 1.0f} },
//	{ { 0.0f,  1.0f,  0.0f}, {1.0f, 1.0f, 0.0f, 1.0f} },
//	{ { 0.0f, -1.0f,  0.0f}, {1.0f, 0.0f, 1.0f, 1.0f} }
//};

//unsigned short cuadroIndices[] = {
//	0, 4, 1,
//	1, 4, 2,
//	2, 4, 3,
//	3, 4, 0,
//	0, 1, 5,
//	1, 2, 5,
//	2, 3, 5,
//	3, 0, 5
//};