#pragma once
#include <vector>
#include <memory>
#include <mutex>
#include "BasePosModel.h"

class CSegment : public CBasePosModel
{
public:
	~CSegment(void)		{ }

	static CSegment *getInstance();
	void InsertModel(float x0, float y0, float x1, float y1);
	phyVector3D getColor() override { return{ 0.7294f, 0.8902f, 1.0f }; }

	void Release() override;

private:
	CSegment(void);

private:
	static std::mutex _mutex;

};
