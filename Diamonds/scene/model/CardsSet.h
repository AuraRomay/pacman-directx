#pragma once
#include <vector>
#include <map>
#include <memory>
#include <mutex>
#include "Card.h"

class CCardsSet
{
public:
	~CCardsSet(void)		{ }

	static CCardsSet *getInstance();

	void Initialize(ID3D11Device *device, ID3D11DeviceContext *context);
	void InsertModel(int index, /*float x, float y, float z, */float wp, float hp,
		float u, float v, float wt, float ht);

	void drawModels();
	void Release();

private:
	CCardsSet(void);

private:
	ID3D11Device *mpDevice;
	ID3D11DeviceContext *mpContext;

	std::map<int, CCard*> mmCards;
	static std::mutex _mutex;

};
