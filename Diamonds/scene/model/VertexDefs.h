#ifndef VERTEXDEFS_H
#define VERTEXDEFS_H

struct BasicTexVertex
{
	float pos[3];
	float tex[2];
};

struct BasicPosVertex
{
	float pos[3];
};

struct TextureVertex
{
	float pos[3];
	float normal[3];
	float texCoord[2];
};

struct KeyframeVertex
{
	float pos[4];
	float normal[3];
	float texCoord[2];
	float nxtPos[3];
	float nxtNormal[3];
};

struct NormalMapVertex
{
	float pos[3];
	float texCoord[2];
	float normal[3];
	float tangent[4];
};

#endif
