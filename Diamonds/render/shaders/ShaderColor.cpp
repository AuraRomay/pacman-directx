#include <fstream>
#include <string>
#include <vector>
#include <d3d11.h>

#include "ShaderColor.h"
#include "../../scene/model/BasePosModel.h"
#include "../DXCamera.h"

//ID3D11VertexShader *ShaderColor::mpVertexShader = nullptr;
//ID3D11PixelShader *ShaderColor::mpPixelShader = nullptr;

ShaderColor::ShaderColor(ID3D11Device *device, ID3D11DeviceContext *context)
: m_pDevice(device), mpContext(context)
, mpVConstantBuffer(nullptr)
, mpPConstantBuffer(nullptr)
, mpInputLayout(0)
{
}

ShaderColor::~ShaderColor(void) 
{
	for (auto obj : mvObjects) {
		obj->Release();
		delete obj;
	}
}

bool ShaderColor::Initialize(
	char *shadername, 
	const D3D11_INPUT_ELEMENT_DESC *vertexld, 
	unsigned int vldsize) 
	//ID3D11Device *device)
{
	int size = 0;
	std::string sname("data/");
	sname.append(shadername);

	std::string vsName = sname + "_VS.cso";
	std::string psName = sname + "_PS.cso";
	std::vector<char> comShader;
	
	FillVectorWithFile(vsName, comShader, size);
	HRESULT hr = m_pDevice->CreateVertexShader(
		&comShader[0], size, 0, &mpVertexShader);

	hr = m_pDevice->CreateInputLayout(
		vertexld, vldsize, 
		&comShader[0], size, &mpInputLayout);

	FillVectorWithFile(psName, comShader, size);
	hr = m_pDevice->CreatePixelShader(
		&comShader[0], size, 0, &mpPixelShader);

	//----------------------------------ConstantBuffer 
	D3D11_BUFFER_DESC constantBufferDesc = {0};
	constantBufferDesc.ByteWidth = sizeof(sVConstantBuffer);
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	hr = m_pDevice->CreateBuffer(
		&constantBufferDesc, 0,	&mpVConstantBuffer);

	mVConstantBufferData.model = MathUtil::MatrixIdentity();
	mVConstantBufferData.view = MathUtil::MatrixIdentity();
	mVConstantBufferData.projection = MathUtil::MatrixIdentity();

	//--
	constantBufferDesc = { 0 };
	constantBufferDesc.ByteWidth = sizeof(mPConstantBufferData);
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	hr = m_pDevice->CreateBuffer(
		&constantBufferDesc, 0, &mpPConstantBuffer);

	for (int i = 0; i < 4; i++)
		mPConstantBufferData.color[1] = 0.f;

	return true;
}

void ShaderColor::updateShaderGlobals()
{
	//ID3D11VertexShader *vshader = mpColorShader.GetVertexShader();
	//ID3D11PixelShader *pshader = mpColorShader.GetPixelShader();
	mpContext->VSSetShader(mpVertexShader, nullptr, 0);
	mpContext->PSSetShader(mpPixelShader, nullptr, 0);

	//ID3D11Buffer *conbuf = mpVConstantBuffer;// mpColorShader.GetVConstantBuffer();
	//sVConstantBuffer &bufvdat = mpColorShader.GetVConstantBufferData();

	MathUtil::CMatrix nview = DXCamera::getInstance()->GetView();
	MathUtil::CMatrix nproj = DXCamera::getInstance()->GetProjection();

	mVConstantBufferData.view = MathUtil::MatrixTranspose(nview);
	mVConstantBufferData.projection = MathUtil::MatrixTranspose(nproj);

	mpContext->UpdateSubresource(
		mpVConstantBuffer, 0, nullptr, &mVConstantBufferData, 0, 0);
	mpContext->VSSetConstantBuffers(0, 1, &mpVConstantBuffer);
	//---


	//for (auto object : mvObjects) {
	//	object->setRenderProperties(this);
	//}


	//conbuf = mpPConstantBuffer;// mpColorShader.GetPConstantBuffer();
	////sPConstantBuffer &bufpdat = mpColorShader.GetPConstantBufferData();
	//phyVector3D color = getColor();
	//mPConstantBufferData.color[0] = color.x;
	//mPConstantBufferData.color[1] = color.y;
	//mPConstantBufferData.color[2] = color.z;
	//mPConstantBufferData.color[3] = 1.f;

	//mpContext->UpdateSubresource(
	//	conbuf, 0, nullptr, &mPConstantBufferData, 0, 0);
	//mpContext->PSSetConstantBuffers(0, 1, &conbuf);


	 //    TODO
	//ID3D11InputLayout *inlay = mpColorShader.GetInputLayout();
	//mpContext->IASetInputLayout(mpInputLayout);
}

void ShaderColor::updateShaderVariables(phyVector3D color)
{
	//conbuf = mpPConstantBuffer;// mpColorShader.GetPConstantBuffer();
														 //sPConstantBuffer &bufpdat = mpColorShader.GetPConstantBufferData();
	//phyVector3D color = getColor();
	mPConstantBufferData.color[0] = color.x;
	mPConstantBufferData.color[1] = color.y;
	mPConstantBufferData.color[2] = color.z;
	mPConstantBufferData.color[3] = 1.f;

	mpContext->UpdateSubresource(
		mpPConstantBuffer, 0, nullptr, &mPConstantBufferData, 0, 0);
	mpContext->PSSetConstantBuffers(0, 1, &mpPConstantBuffer);

	//ID3D11InputLayout *inlay = mpColorShader.GetInputLayout();
	mpContext->IASetInputLayout(mpInputLayout);
}

void ShaderColor::Update()
{
	updateShaderGlobals();
	for (auto object : mvObjects) {
		object->setRenderProperties(this);
		object->drawModel();
	}
}

