#ifndef SHADERBASE_H
#define SHADERBASE_H

#include "VertexFormat.h"
#include <vector>

struct ID3D11Device;
struct ID3D11InputLayout;

struct ID3D11VertexShader;
struct ID3D11PixelShader;


struct ID3D11Buffer;

using namespace std;

class ShaderBase
{
protected:
	ID3D11InputLayout	*mpInputLayout;

	static ID3D11VertexShader		*mpVertexShader;

	static ID3D11PixelShader		*mpPixelShader;

	ID3D11Buffer		*mpConstantBuffer;
	ID3D11Buffer		*mpAnimationBuffer;
	sVConstantBuffer	mConstantBufferData;
	sAnimationCBuffer	mAnimationBufferData;

	unsigned long m_dwShaderFlags;

public:
	ShaderBase(void);
	virtual ~ShaderBase(void) { }

	bool Initialize(
		char *shadername,
		const D3D11_INPUT_ELEMENT_DESC *vertexld,
		unsigned int vldsize,	ID3D11Device *device);

	ID3D11Buffer *GetVConstantBuffer()						{ return mpConstantBuffer; }
	ID3D11Buffer *GetVAnimationBuffer()						{ return mpAnimationBuffer; }
	sVConstantBuffer &GetVConstantBufferData()		{ return mConstantBufferData; }
	sAnimationCBuffer &GetVAnimationtBufferData()	{ return mAnimationBufferData; }

	ID3D11VertexShader *GetVertexShader()		{ return mpVertexShader; }
	ID3D11PixelShader *GetPixelShader()			{ return mpPixelShader; }

	ID3D11InputLayout *GetInputLayout()			{ return mpInputLayout; }

};

#endif
