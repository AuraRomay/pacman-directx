#pragma once

#include "VertexFormat.h"
#include <vector>

struct ID3D11Device;
struct ID3D11DeviceContext;

struct ID3D11InputLayout;

struct ID3D11VertexShader;
struct ID3D11PixelShader;


struct ID3D11Buffer;

class CBasePosModel;

using namespace std;

class ShaderColor
{
public:
	ShaderColor(ID3D11Device *device, ID3D11DeviceContext *context);
	~ShaderColor(void);

	bool Initialize(
		char *shadername,
		const D3D11_INPUT_ELEMENT_DESC *vertexld,
		unsigned int vldsize);// , ID3D11Device *device);

	void insertObjectToShader(CBasePosModel *obj) { mvObjects.push_back(obj); }

	void updateShaderGlobals();
	void updateShaderVariables(phyVector3D color);

	void Update();

	//ID3D11Buffer *GetVConstantBuffer()					{ return mpVConstantBuffer; }
	//sVConstantBuffer &GetVConstantBufferData()	{ return mVConstantBufferData; }

	//ID3D11Buffer *GetPConstantBuffer()					{ return mpPConstantBuffer; }
	//sPConstantBuffer &GetPConstantBufferData()	{ return mPConstantBufferData; }

	//ID3D11VertexShader *GetVertexShader()		{ return mpVertexShader; }

	//ID3D11PixelShader *GetPixelShader()			{ return mpPixelShader; }

	//ID3D11InputLayout *GetInputLayout()			{ return mpInputLayout; }

protected:
	ID3D11Device		*m_pDevice;
	ID3D11DeviceContext *mpContext;

	ID3D11InputLayout	*mpInputLayout;

	ID3D11VertexShader		*mpVertexShader;
	ID3D11PixelShader		*mpPixelShader;

	ID3D11Buffer		*mpVConstantBuffer;
	ID3D11Buffer		*mpPConstantBuffer;
	sVConstantBuffer		mVConstantBufferData;
	sPConstantBuffer		mPConstantBufferData;

	unsigned long m_dwShaderFlags;

	std::vector<CBasePosModel *> mvObjects;



};
