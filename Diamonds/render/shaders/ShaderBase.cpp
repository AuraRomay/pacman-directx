#include <fstream>
#include <string>
#include <vector>
#include <d3d11.h>

#include "ShaderBase.h"

ID3D11VertexShader *ShaderBase::mpVertexShader = nullptr;
ID3D11PixelShader *ShaderBase::mpPixelShader = nullptr;

ShaderBase::ShaderBase(void)
: mpConstantBuffer(nullptr)
, mpAnimationBuffer(nullptr)
, mpInputLayout(nullptr)
{
}

bool ShaderBase::Initialize(
	char *shadername, 
	const D3D11_INPUT_ELEMENT_DESC *vertexld, 
	unsigned int vldsize, 
	ID3D11Device *device)
{
	int size = 0;
	std::string sname("data/");
	sname.append(shadername);

	std::string vsName = sname + "_VS.cso";
	std::string psName = sname + "_PS.cso";
	std::vector<char> comShader;
	
	FillVectorWithFile(vsName, comShader, size);
	HRESULT hr = device->CreateVertexShader(
		&comShader[0], size, 0, &mpVertexShader);
	if (hr != S_OK) { return false; }

	hr = device->CreateInputLayout(
		vertexld, vldsize, 
		&comShader[0], size, &mpInputLayout);
	if (hr != S_OK) { return false; }

	FillVectorWithFile(psName, comShader, size);
	hr = device->CreatePixelShader(
		&comShader[0], size, 0, &mpPixelShader);
	if (hr != S_OK) { return false; }

	//-------------------------------------ConstantBuffer 
	D3D11_BUFFER_DESC constantBufferDesc = {0};
	//constantBufferDesc.ByteWidth = sizeof(mConstantBufferData);
	constantBufferDesc.ByteWidth = sizeof(sVConstantBuffer);
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	hr = device->CreateBuffer(
		&constantBufferDesc, 0,	&mpConstantBuffer);
	if (hr != S_OK) { return false; }

	D3D11_BUFFER_DESC animaBufferDesc = { 0 };
	animaBufferDesc.ByteWidth = sizeof(sAnimationCBuffer);
	animaBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	animaBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	animaBufferDesc.CPUAccessFlags = 0;
	animaBufferDesc.MiscFlags = 0;
	animaBufferDesc.StructureByteStride = 0;

	hr = device->CreateBuffer(
		&animaBufferDesc, 0, &mpAnimationBuffer);
	if (hr != S_OK) { return false; }

	mConstantBufferData.model = MathUtil::MatrixIdentity();
	mConstantBufferData.view = MathUtil::MatrixIdentity();
	mConstantBufferData.projection = MathUtil::MatrixIdentity();
	mAnimationBufferData = { { 0.f, 0.f }, { 0.f, 0.f } };
	return true;
}




