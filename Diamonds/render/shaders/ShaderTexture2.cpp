#include <fstream>
#include <string>
#include <vector>
#include <d3d11.h>

#include "ShaderTexture2.h"
#include "../../scene/model/TextureModel2.h"
#include "../DXCamera.h"
#include "../../directxtk/include/DDSTextureLoader.h"

//ID3D11VertexShader *ShaderColor::mpVertexShader = nullptr;
//ID3D11PixelShader *ShaderColor::mpPixelShader = nullptr;

ShaderTexture2::ShaderTexture2(ID3D11Device *device, ID3D11DeviceContext *context)
: m_pDevice(device), mpContext(context)
, mpVConstantBuffer(nullptr)
//, mpPConstantBuffer(nullptr)
, mpSampleState(nullptr)
, mpInputLayout(0)
{
}

ShaderTexture2::~ShaderTexture2(void)
{
	for (auto obj : mvObjects) {
		obj->Release();
		delete obj;
	}
}

bool ShaderTexture2::Initialize( char *shadername, 
	const D3D11_INPUT_ELEMENT_DESC *vertexld, 
	unsigned int vldsize)
{
	int size = 0;
	std::string sname("data/");
	sname.append(shadername);

	std::string vsName = sname + "_VS.cso";
	std::string psName = sname + "_PS.cso";
	std::vector<char> comShader;
	
	FillVectorWithFile2(vsName, comShader, size);
	HRESULT hr = m_pDevice->CreateVertexShader(
		&comShader[0], size, 0, &mpVertexShader);

	hr = m_pDevice->CreateInputLayout(
		vertexld, vldsize, 
		&comShader[0], size, &mpInputLayout);

	FillVectorWithFile2(psName, comShader, size);
	hr = m_pDevice->CreatePixelShader(
		&comShader[0], size, 0, &mpPixelShader);

	//----------------------------------ConstantBuffer 
	D3D11_BUFFER_DESC constantBufferDesc = {0};
	constantBufferDesc.ByteWidth = sizeof(sVConstantBuffer2);
	constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags = 0;
	constantBufferDesc.MiscFlags = 0;
	constantBufferDesc.StructureByteStride = 0;

	hr = m_pDevice->CreateBuffer(
		&constantBufferDesc, 0,	&mpVConstantBuffer);

	mVConstantBufferData.model = MathUtil::MatrixIdentity();
	mVConstantBufferData.view = MathUtil::MatrixIdentity();
	mVConstantBufferData.projection = MathUtil::MatrixIdentity();

	//--
	D3D11_SAMPLER_DESC samplerDesc;
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 1;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	hr = m_pDevice->CreateSamplerState(&samplerDesc, &mpSampleState);

	DirectX::CreateDDSTextureFromFile(m_pDevice, L"data/amarillo.dds", nullptr, &mTextures[0]);
	DirectX::CreateDDSTextureFromFile(m_pDevice, L"data/maze.dds", nullptr, &mTextures[1]);
	DirectX::CreateDDSTextureFromFile(m_pDevice, L"data/balls.dds", nullptr, &mTextures[2]);

	//constantBufferDesc = { 0 };
	//constantBufferDesc.ByteWidth = sizeof(mPConstantBufferData);
	//constantBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	//constantBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	//constantBufferDesc.CPUAccessFlags = 0;
	//constantBufferDesc.MiscFlags = 0;
	//constantBufferDesc.StructureByteStride = 0;

	//hr = m_pDevice->CreateBuffer(
	//	&constantBufferDesc, 0, &mpPConstantBuffer);

	//for (int i = 0; i < 4; i++)
	//	mPConstantBufferData.color[1] = 0.f;

	return true;
}

void ShaderTexture2::updateShaderGlobals()
{
	mpContext->VSSetShader(mpVertexShader, nullptr, 0);
	mpContext->PSSetShader(mpPixelShader, nullptr, 0);

	ID3D11Buffer *conbuf = mpVConstantBuffer;

	MathUtil::CMatrix nview = DXCamera::getInstance()->GetView();
	MathUtil::CMatrix nproj = DXCamera::getInstance()->GetProjection();

	mVConstantBufferData.view = MathUtil::MatrixTranspose(nview);
	mVConstantBufferData.projection = MathUtil::MatrixTranspose(nproj);

	mpContext->UpdateSubresource(
		conbuf, 0, nullptr, &mVConstantBufferData, 0, 0);
	mpContext->VSSetConstantBuffers(0, 1, &conbuf);
}

void ShaderTexture2::updateShaderVariables(phyVector3D pos, phyVector3D sca, int ind)
{
	//mPConstantBufferData.color[0] = color.x;
	//mPConstantBufferData.color[1] = color.y;
	//mPConstantBufferData.color[2] = color.z;
	//mPConstantBufferData.color[3] = 1.f;

	//mpContext->UpdateSubresource(
	//	mpPConstantBuffer, 0, nullptr, &mPConstantBufferData, 0, 0);
	//mpContext->PSSetConstantBuffers(0, 1, &mpPConstantBuffer);
	auto nodelm = MathUtil::MatrixTranslation(pos.x, pos.y, pos.z);
	auto mod = MathUtil::MatrixScale(sca.x, sca.y, sca.z);
	mod = MathUtil::Multiply(mod, nodelm);
	mVConstantBufferData.model = MathUtil::MatrixTranspose(mod);

	mpContext->UpdateSubresource(
		mpVConstantBuffer, 0, nullptr, &mVConstantBufferData, 0, 0);
	mpContext->VSSetConstantBuffers(0, 1, &mpVConstantBuffer);

	mpContext->PSSetSamplers(0, 1, &mpSampleState);

	mpContext->PSSetShaderResources(0, 1, &mTextures[ind]);
	//mpContext->PSSetShaderResources(0, 1, &mTextures[1]);

	/*mpContext->PSSetShaderResources(0, 2, mTextures);
*/
	mpContext->IASetInputLayout(mpInputLayout);
}

void ShaderTexture2::Update()
{
	updateShaderGlobals();
	for (auto object : mvObjects) {
		object->setRenderProperties(this);
		object->drawModel();
	}
}

