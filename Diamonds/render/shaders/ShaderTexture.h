#pragma once

#include "VertexFormat.h"
#include <vector>

struct ID3D11Device;
struct ID3D11DeviceContext;

struct ID3D11InputLayout;

struct ID3D11VertexShader;
struct ID3D11PixelShader;


struct ID3D11Buffer;

class CTextureModel;

using namespace std;

class ShaderTexture
{
public:
	ShaderTexture(ID3D11Device *device, ID3D11DeviceContext *context);
	~ShaderTexture(void);

	bool Initialize(
		char *shadername,
		const D3D11_INPUT_ELEMENT_DESC *vertexld,
		unsigned int vldsize);

	void insertObjectToShader(CTextureModel *obj) { mvObjects.push_back(obj); }

	void updateShaderGlobals();
	void updateShaderVariables(phyVector3D position, phyVector3D rotAngles, int ind);

	void Update();

protected:
	ID3D11Device		*m_pDevice;
	ID3D11DeviceContext *mpContext;

	ID3D11InputLayout	*mpInputLayout;

	ID3D11VertexShader		*mpVertexShader;
	ID3D11PixelShader		*mpPixelShader;

	ID3D11Buffer		*mpVConstantBuffer;
	ID3D11Buffer		*mpVAnimCBuffer;

	//ID3D11Buffer		*mpPConstantBuffer;

	ID3D11SamplerState *mpSampleState;

	sVConstantBuffer		mVConstantBufferData;
	//sPConstantBuffer		mPConstantBufferData;

	ID3D11ShaderResourceView* mTextures[4];

	unsigned long m_dwShaderFlags;

	std::vector<CTextureModel *> mvObjects;



};
