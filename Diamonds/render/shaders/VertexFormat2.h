#pragma once
#include <d3d11.h>
#include <vector>
#include <fstream>
#include "../../scene/logic/UMath.h"

const D3D11_INPUT_ELEMENT_DESC basicVertexLayoutDesc2[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	//{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

const D3D11_INPUT_ELEMENT_DESC posVertexLayoutDesc2 [] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

//const D3D11_INPUT_ELEMENT_DESC textureVertexLayoutDesc[] =
//{
//	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
//};

const D3D11_INPUT_ELEMENT_DESC textureVertexLayoutDesc2[] =
{
	{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
};

const D3D11_INPUT_ELEMENT_DESC cubistVertexLayoutDesc2[] =
{
	{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
	{"SIZE", 0, DXGI_FORMAT_R32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
};

struct sVConstantBuffer2
{
		MathUtil::CMatrix  model;
		MathUtil::CMatrix  view;
		MathUtil::CMatrix  projection;
};

struct sAnimationCBuffer2
{
	float uOffset[2];
	float padding[2];
};

struct sPConstantBuffer2
{
	float color[4];
};

static void FillVectorWithFile2(const std::string &name,
	std::vector<char> &mvec, int &size)
{
	std::ifstream fin(name, std::ios::binary);
	fin.seekg(0, std::ios_base::end);
	int szvec = static_cast<int>(fin.tellg());
	fin.seekg(0, std::ios_base::beg);

	mvec.clear();
	mvec.resize(szvec);
	fin.read(&mvec[0], szvec);
	fin.close();

	size = szvec;
}

